
package cn.edu.bjtu.timeseriesalgorithm.utilities;

import cn.edu.bjtu.timeseriesalgorithm.weka.core.Instances;

/**
 *
 * @author ajb
 */
public interface SaveParameterInfo {
    String getParameters();
    
}
