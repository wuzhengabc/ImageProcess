/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.edu.bjtu.timeseriesalgorithm.multivariate_timeseriesweka.classifiers;

import cn.edu.bjtu.timeseriesalgorithm.multivariate_timeseriesweka.measures.EuclideanDistance_I;
import static cn.edu.bjtu.timeseriesalgorithm.utilities.InstanceTools.findMinDistance;
import cn.edu.bjtu.timeseriesalgorithm.utilities.generic_storage.Pair;
import cn.edu.bjtu.timeseriesalgorithm.weka.classifiers.AbstractClassifier;
import cn.edu.bjtu.timeseriesalgorithm.weka.core.Instance;
import cn.edu.bjtu.timeseriesalgorithm.weka.core.Instances;

/**
 *
 * @author Aaron
 */
public class NN_ED_I extends AbstractClassifier{
    
    Instances train;
    EuclideanDistance_I I;
    public NN_ED_I(){
        I = new EuclideanDistance_I();
    }

    @Override
    public void buildClassifier(Instances data) throws Exception {
        train = data;
    }
    
    @Override
    public double classifyInstance(Instance instance) throws Exception{
        Pair<Instance, Double> minD = findMinDistance(train, instance, I);
        return minD.var1.classValue();
    }
}
