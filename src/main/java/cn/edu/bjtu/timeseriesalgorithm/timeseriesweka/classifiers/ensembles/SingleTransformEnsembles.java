package cn.edu.bjtu.timeseriesalgorithm.timeseriesweka.classifiers.ensembles;
import java.util.ArrayList;
import java.util.Random;
import cn.edu.bjtu.timeseriesalgorithm.weka.attributeSelection.PrincipalComponents;
import cn.edu.bjtu.timeseriesalgorithm.weka.classifiers.AbstractClassifier;
import cn.edu.bjtu.timeseriesalgorithm.weka.classifiers.Classifier;
import cn.edu.bjtu.timeseriesalgorithm.weka.classifiers.Evaluation;
import cn.edu.bjtu.timeseriesalgorithm.timeseriesweka.classifiers.FastDTW_1NN;
import cn.edu.bjtu.timeseriesalgorithm.weka.classifiers.lazy.kNN;
import cn.edu.bjtu.timeseriesalgorithm.weka.core.Instance;
import cn.edu.bjtu.timeseriesalgorithm.weka.core.Instances;
import cn.edu.bjtu.timeseriesalgorithm.weka.filters.NormalizeAttribute;
import cn.edu.bjtu.timeseriesalgorithm.weka.filters.NormalizeCase;
import cn.edu.bjtu.timeseriesalgorithm.weka.filters.SimpleBatchFilter;

public class SingleTransformEnsembles extends AbstractClassifier{


    enum TransformType {TIME,PS,ACF}; 
    TransformType t = TransformType.TIME;
    SimpleBatchFilter transform;
    Classifier[] classifiers;
    Instances train;

    public SingleTransformEnsembles(){
        super();
        initialise();
    }
    public final void initialise(){
//Transform            
        switch(t){
            case TIME:
                transform=new NormalizeCase();
                break;
                

        }

    }
    @Override
    public void buildClassifier(Instances data){
        
    }        
  
	@Override
	public String getRevision() {
		// TODO Auto-generated method stub
		return null;
	}
	public static void main(String[] args){
//Load up Beefand test only on that
		
		
	}
	
}
