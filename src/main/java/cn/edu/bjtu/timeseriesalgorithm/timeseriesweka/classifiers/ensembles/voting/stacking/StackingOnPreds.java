/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.edu.bjtu.timeseriesalgorithm.timeseriesweka.classifiers.ensembles.voting.stacking;

import cn.edu.bjtu.timeseriesalgorithm.weka.classifiers.Classifier;
import cn.edu.bjtu.timeseriesalgorithm.timeseriesweka.classifiers.ensembles.EnsembleModule;
import cn.edu.bjtu.timeseriesalgorithm.weka.core.DenseInstance;
import cn.edu.bjtu.timeseriesalgorithm.weka.core.Instance;

/**
 *
 * @author James Large james.large@uea.ac.uk
 */
public class StackingOnPreds extends AbstractStacking {
    
    public StackingOnPreds(Classifier classifier) {
        super(classifier);
    }
    
    public StackingOnPreds(Classifier classifier, int numClasses) {
        super(classifier, numClasses);
    }
    
    @Override
    protected void setNumOutputAttributes(EnsembleModule[] modules) {
        this.numOutputAtts = modules.length + 1; //each pred + class val
    }
    
    @Override
    protected Instance buildInst(double[][] dists, Double classVal) throws Exception {
        double[] instData = new double[numOutputAtts];
        
        for (int m = 0; m < dists.length; m++) 
            instData[m] = indexOfMax(dists[m]);
        
        if (classVal != null)
            instData[numOutputAtts-1] = classVal; 
        //else irrelevent 
        
        instsHeader.add(new DenseInstance(1.0, instData));
        return instsHeader.remove(0);
    }

}
