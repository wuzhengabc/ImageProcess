package cn.edu.bjtu.imageprocess.dto;

import cn.edu.bjtu.imageprocess.util.DateProcessing;

public class OtherEvaItem {

 double kappa;
 double meanAbsErr;
 double precision;
 double recall;
 double auc;
 double correct;
 public double getAuc() {
	return auc;
}
public void setAuc(double auc) {
	this.auc = auc;
}
public double getCorrect() {
	return correct;
}
public void setCorrect(double correct) {
	this.correct = correct;
}
public double getIncorrect() {
	return incorrect;
}
public void setIncorrect(double incorrect) {
	this.incorrect = incorrect;
}
double incorrect;
 public OtherEvaItem( double kappa,double meanAbsErr,double precision,double recall, double auc,double correct,double incorrect){
	 DateProcessing d = DateProcessing.get();
	 int digits = 4;
	 this.kappa = d.decimalDigits(kappa, digits);
	 this.meanAbsErr = d.decimalDigits(meanAbsErr, digits);
	 this.precision = d.decimalDigits(precision, digits);
	 this.recall = d.decimalDigits(recall, digits);
	 this.auc = d.decimalDigits(auc, digits);
	 this.correct = d.decimalDigits(correct/(correct+incorrect)*100, 2);
	 this.incorrect = d.decimalDigits(incorrect/(correct+incorrect)*100, 2);
 }
 public double getKappa() {
	return kappa;
}
public void setKappa(double kappa) {
	this.kappa = kappa;
}
public double getMeanAbsErr() {
	return meanAbsErr;
}
public void setMeanAbsErr(double meanAbsErr) {
	this.meanAbsErr = meanAbsErr;
}
public double getPrecision() {
	return precision;
}
public void setPrecision(double precision) {
	this.precision = precision;
}
public double getRecall() {
	return recall;
}
public void setRecall(double recall) {
	this.recall = recall;
}


 
}
