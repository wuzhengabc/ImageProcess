/**
 * 2017年8月18日
 */
package cn.edu.bjtu.imageprocess.dto;

/**
 * @author Alex
 *
 */
public final class FileItem {
	String name;
	/**
	 * @param name
	 * @param size
	 */
	long size;
	public FileItem(String name, long size) {
		super();
		this.name = name;
		this.size = size;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	
}
