package cn.edu.bjtu.imageprocess.dto;
/**
 * @author LMR
 *
 */
public class RocItem {

	double tpRate;
	/**
	 * @param tpRate
	 * @param fpRate
	 */
	public RocItem(double tpRate,double fpRate){
		this.tpRate = tpRate;
		this.fpRate = fpRate;
	}
	public double getTpRate() {
		return tpRate;
	}
	public void setTpRate(double tpRate) {
		this.tpRate = tpRate;
	}
	public double getFpRate() {
		return fpRate;
	}
	public void setFpRate(double fpRate) {
		this.fpRate = fpRate;
	}
	double fpRate;
}
