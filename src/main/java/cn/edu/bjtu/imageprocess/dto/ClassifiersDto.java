package cn.edu.bjtu.imageprocess.dto;

import java.util.List;

public class ClassifiersDto {

	List<ClassifiersFile> data;

	public ClassifiersDto(List<ClassifiersFile> data){
		
		this.data = data;
	}
	public List<ClassifiersFile> getData() {
		return data;
	}

	public void setData(List<ClassifiersFile> data) {
		this.data = data;
	}
}
