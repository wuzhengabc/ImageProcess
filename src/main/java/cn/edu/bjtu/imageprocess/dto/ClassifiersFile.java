package cn.edu.bjtu.imageprocess.dto;

public final class ClassifiersFile {
	/**
	 * @param classifiersName
	 * @param algorithmName
	 * @param TrainFileName
	 */
	String classifiersName;
	String algorithmName;
	
	
	public ClassifiersFile(String classifiersName,String algorithmName){
		super();
		this.classifiersName = classifiersName;
		this.algorithmName = algorithmName;
		//this.TrainFileName = TrainFileName;
	}
	
	public String getClassifiersName() {
		return classifiersName;
	}
	public void setClassifiersName(String classifiersName) {
		this.classifiersName = classifiersName;
	}
	public String getAlgorithmName() {
		return algorithmName;
	}
	public void setAlgorithmName(String algorithmName) {
		this.algorithmName = algorithmName;
	}
	

}
