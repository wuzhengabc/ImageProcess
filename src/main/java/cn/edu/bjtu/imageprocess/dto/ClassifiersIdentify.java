package cn.edu.bjtu.imageprocess.dto;

/** 
* @author 作者 LMR
* @version 创建时间：2017年10月22日 下午10:16:13 
*/

public class ClassifiersIdentify implements Comparable<ClassifiersIdentify>{
	String key;
	String value;

	public ClassifiersIdentify(String key,String value){
		this.key =key;
		this.value =value;
	}
	public int compareTo(ClassifiersIdentify o) {
		return key.compareTo(o.key)>=0?1:-1;
	}
	public String hashCodes(){
		return key+"-"+value;
	}
}
