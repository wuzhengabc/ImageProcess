/**
 * 2017年8月18日
 */
package cn.edu.bjtu.imageprocess.dto;

import java.util.List;

/**
 * @author Alex
 *
 */

public class ArffListDto {
	List<FileItem> aaData;

	/**
	 * @param aaData
	 */
	public ArffListDto(List<FileItem> aaData) {
		super();
		this.aaData = aaData;
	}

	public List<FileItem> getAaData() {
		return aaData;
	}

	public void setAaData(List<FileItem> aaData) {
		this.aaData = aaData;
	}
	
}
