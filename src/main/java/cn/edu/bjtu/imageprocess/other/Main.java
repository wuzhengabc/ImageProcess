/**
 * 2017年8月15日
 */
package cn.edu.bjtu.imageprocess.other;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.web.context.support.StandardServletEnvironment;

import cn.edu.bjtu.imageprocess.App;

/**
 * @author Alex
 *
 */
public class Main {
	public static void main(String[] args) {
		ConfigurableEnvironment env = new StandardServletEnvironment();
		ConfigurableApplicationContext cac = new AnnotationConfigEmbeddedWebApplicationContext();
		cac.setEnvironment(env);
		Object sources = App.class;
		BeanDefinitionLoader loader = createBeanDefinitionLoader(getBeanDefinitionRegistry(cac), sources);
		loader.load();
		cac.refresh();
	}



	/**
	 * @param beanDefinitionRegistry
	 * @param sources
	 * @return
	 */
	private static cn.edu.bjtu.imageprocess.other.BeanDefinitionLoader createBeanDefinitionLoader(
			Object beanDefinitionRegistry, Object sources) {
		return new BeanDefinitionLoader((BeanDefinitionRegistry)beanDefinitionRegistry,sources);
	}



	/**
	 * @param cac
	 * @return
	 */
	private static Object getBeanDefinitionRegistry(ConfigurableApplicationContext cac) {
		return cac;
	}
	
	
}
