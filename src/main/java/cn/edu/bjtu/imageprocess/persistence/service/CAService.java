/**
 * 2017年8月15日
 */
package cn.edu.bjtu.imageprocess.persistence.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import cn.edu.bjtu.imageprocess.persistence.dao.CACurdRepository;
import cn.edu.bjtu.imageprocess.persistence.entity.CategoryAttribute;

/**
 * @author Alex
 *
 */
@Service
public class CAService {
	@Autowired  CACurdRepository caRepo;
	/**
	 * @param entity
	 * @return
	 * @see org.springframework.data.repository.CrudRepository#save(java.lang.Object)
	 */
	public <S extends CategoryAttribute> S save(S entity) {
		return caRepo.save(entity);
	}
	/**
	 * @return
	 * @see org.springframework.data.jpa.repository.JpaRepository#findAll()
	 */
	public List<CategoryAttribute> findAll() {
		return caRepo.findAll();
	}
	/**
	 * @param sort
	 * @return
	 * @see org.springframework.data.jpa.repository.JpaRepository#findAll(org.springframework.data.domain.Sort)
	 */
	public List<CategoryAttribute> findAll(Sort sort) {
		return caRepo.findAll(sort);
	}
	/**
	 * @param pageable
	 * @return
	 * @see org.springframework.data.repository.PagingAndSortingRepository#findAll(org.springframework.data.domain.Pageable)
	 */
	public Page<CategoryAttribute> findAll(Pageable pageable) {
		return caRepo.findAll(pageable);
	}
	/**
	 * @param id
	 * @return
	 * @see org.springframework.data.repository.CrudRepository#findOne(java.io.Serializable)
	 */
	public CategoryAttribute findOne(Long id) {
		return caRepo.findOne(id);
	}
	/**
	 * @param ids
	 * @return
	 * @see org.springframework.data.jpa.repository.JpaRepository#findAll(java.lang.Iterable)
	 */
	public List<CategoryAttribute> findAll(Iterable<Long> ids) {
		return caRepo.findAll(ids);
	}
	/**
	 * @param entities
	 * @return
	 * @see org.springframework.data.jpa.repository.JpaRepository#save(java.lang.Iterable)
	 */
	public <S extends CategoryAttribute> List<S> save(Iterable<S> entities) {
		return caRepo.save(entities);
	}
	/**
	 * @param id
	 * @return
	 * @see org.springframework.data.repository.CrudRepository#exists(java.io.Serializable)
	 */
	public boolean exists(Long id) {
		return caRepo.exists(id);
	}
	/**
	 * 
	 * @see org.springframework.data.jpa.repository.JpaRepository#flush()
	 */
	public void flush() {
		caRepo.flush();
	}
	/**
	 * @param entity
	 * @return
	 * @see org.springframework.data.jpa.repository.JpaRepository#saveAndFlush(java.lang.Object)
	 */
	public <S extends CategoryAttribute> S saveAndFlush(S entity) {
		return caRepo.saveAndFlush(entity);
	}
	/**
	 * @param entities
	 * @see org.springframework.data.jpa.repository.JpaRepository#deleteInBatch(java.lang.Iterable)
	 */
	public void deleteInBatch(Iterable<CategoryAttribute> entities) {
		caRepo.deleteInBatch(entities);
	}
	/**
	 * @return
	 * @see org.springframework.data.repository.CrudRepository#count()
	 */
	public long count() {
		return caRepo.count();
	}
	/**
	 * 
	 * @see org.springframework.data.jpa.repository.JpaRepository#deleteAllInBatch()
	 */
	public void deleteAllInBatch() {
		caRepo.deleteAllInBatch();
	}
	/**
	 * @param id
	 * @see org.springframework.data.repository.CrudRepository#delete(java.io.Serializable)
	 */
	public void delete(Long id) {
		caRepo.delete(id);
	}
	/**
	 * @param id
	 * @return
	 * @see org.springframework.data.jpa.repository.JpaRepository#getOne(java.io.Serializable)
	 */
	public CategoryAttribute getOne(Long id) {
		return caRepo.getOne(id);
	}
	/**
	 * @param entity
	 * @see org.springframework.data.repository.CrudRepository#delete(java.lang.Object)
	 */
	public void delete(CategoryAttribute entity) {
		caRepo.delete(entity);
	}
	/**
	 * @param entities
	 * @see org.springframework.data.repository.CrudRepository#delete(java.lang.Iterable)
	 */
	public void delete(Iterable<? extends CategoryAttribute> entities) {
		caRepo.delete(entities);
	}
	/**
	 * 
	 * @see org.springframework.data.repository.CrudRepository#deleteAll()
	 */
	public void deleteAll() {
		caRepo.deleteAll();
	}
	@Transactional
	public void method(boolean flag){
		caRepo.save(new CategoryAttribute(3,"a","b","c","d"));
		caRepo.save(new CategoryAttribute(4,"d","e","f","g"));
		if(flag)throw new RuntimeException("test");
		caRepo.save(new CategoryAttribute(6,"dd","ee","ff","gg"));
		
		
	}
}
