/**
 * 2017年8月15日
 */
package cn.edu.bjtu.imageprocess.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.edu.bjtu.imageprocess.persistence.entity.CategoryAttribute;

/**
 * @author Alex
 *
 */
public interface CACurdRepository extends JpaRepository<CategoryAttribute,Long>{


}
