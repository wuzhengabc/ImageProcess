/**
 * 2017年8月15日
 */
package cn.edu.bjtu.imageprocess.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @author Alex
 *
 */
@Entity
@Table(name="cms_category_attribute")
public class CategoryAttribute {
	@Id
	int category_id;
	String title;
	String keywords;
	String description;
	String data;
	public CategoryAttribute() {
	}
	
	

	/**
	 * @param category_id
	 * @param title
	 * @param keywords
	 * @param description
	 * @param data
	 */
	public CategoryAttribute(int category_id, String title, String keywords, String description, String data) {
		super();
		this.category_id = category_id;
		this.title = title;
		this.keywords = keywords;
		this.description = description;
		this.data = data;
	}



	/**
	 * @param title
	 * @param keywords
	 * @param description
	 * @param data
	 */
	public CategoryAttribute(String title, String keywords, String description, String data) {
		super();
		this.title = title;
		this.keywords = keywords;
		this.description = description;
		this.data = data;
	}


	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
}
