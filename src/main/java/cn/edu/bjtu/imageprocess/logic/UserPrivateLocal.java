/**
 * 2017年8月18日
 */
package cn.edu.bjtu.imageprocess.logic;

import java.io.File;
import java.util.Date;

/**
 * @author Alex
 *
 */
public class UserPrivateLocal {
	Date loginTime;
	File recentOpenFile;
	public Date getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
	public File getRecentOpenFile() {
		return recentOpenFile;
	}
	public void setRecentOpenFile(File recentOpenFile) {
		this.recentOpenFile = recentOpenFile;
	}
	
}
