/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.api;

import org.opencv.core.Mat;

import cn.edu.bjtu.imageprocess.annotation.Stateless;

/**
 * 每个Valve必须调用{@code ValveChain} 中的transformNext或者transformTerminal方法之一
 * 这一个接口的实现类全部是无状态的,因此不需要担心线程安全问题
 * @author Alex
 *
 */
@Stateless
public interface Valve {
	/**
	 * 该方法中必须调用ValveChain中的transformNext或者transformTerminal方法之一,
	 * 否则返回结果异常
	 * 参数的话,像SRC 及 VC就够了,最后的那个otherArgs是有些图像处理流程并不只是一个图像,
	 * 比如寻找质心,得到的结果是一个Point,这种情况Point就通过otherArgs传递给下一步操作
	 * @param src
	 * @param vc
	 */
	void transform(Mat src , ValveChain vc,Object ...otherArgs);
}
