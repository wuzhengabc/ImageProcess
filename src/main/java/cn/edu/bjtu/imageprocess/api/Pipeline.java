/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.api;

import java.util.List;

import org.opencv.core.Mat;

import cn.edu.bjtu.imageprocess.annotation.Stateful;

/**
 * @author Alex
 *
 */
@Stateful
public interface Pipeline {
	void reset();
	List<Mat> getAllResults();
	List<Object[]> getAllOtherResults();
	Object[] getFinalOtherResult();
	Mat getFinalResult();
	ValveChain getValveChain();
	void setInput(Mat m);
	void action();
}
