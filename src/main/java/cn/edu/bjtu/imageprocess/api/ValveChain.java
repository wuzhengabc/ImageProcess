/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.api;

import java.util.List;
import java.util.Map;

import org.opencv.core.Mat;

import cn.edu.bjtu.imageprocess.annotation.Stateful;

/**
 * @author Alex
 *
 */
@Stateful
public interface ValveChain {
	/**
	 * 进行下一步处理
	 * @param src
	 */
	void transformNext(Mat src,Object ... otherArgs);
	/**
	 * 停止下一步处理,并当处理的结果返回给ValveChain,使得调用者得到最后结果
	 * @param
	 */
	Map<String,String> getConfigurationMap();
	ValveChain setConfigurationKeyWithValue(String key,String value);
	void transformTerminal(Mat src, Object ... otherArgs);
	void reset();
	void handleException(/*可能还会有其他参数*/Exception e , Mat m);
	Mat getOutput();
	public List<Mat> getAllResults();
	public List<Object[]> getAllOtherResults();
	public Object[] getFinalOtherResult();
}
