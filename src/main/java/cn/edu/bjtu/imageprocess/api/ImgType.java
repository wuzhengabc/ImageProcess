package cn.edu.bjtu.imageprocess.api;

/**
 * Created by wuzheng on 2017/9/21
 */
public enum ImgType
{
    GIF,
    BMP,
    JPG,
    JPEG,
    PNG
}
