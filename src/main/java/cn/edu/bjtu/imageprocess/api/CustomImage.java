package cn.edu.bjtu.imageprocess.api;

import java.awt.*;

/**
 * Created by wuzheng on 2017/9/14
 */
public interface CustomImage
{
    ImgType getImgType();
    Image getImg();
    int getImgSize();
}
