package cn.edu.bjtu.imageprocess.api;

/**
 * Created by wuzheng on 2017/9/14
 */
public interface ImageSet
{
	int size();
	void add();
	void clear();
	void insert(int index);
	CustomImage delete(int index);
	CustomImage get(int index);
}
