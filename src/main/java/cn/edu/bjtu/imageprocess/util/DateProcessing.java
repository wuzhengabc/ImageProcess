package cn.edu.bjtu.imageprocess.util;

import java.math.BigDecimal;

public class DateProcessing {

	private DateProcessing(){}
	private static final DateProcessing single = new DateProcessing();
	public static  DateProcessing get() {
		return single;
	}
	public double decimalDigits(double data,int digits){
		BigDecimal b = new BigDecimal(data);
		double f1 = b.setScale(digits, BigDecimal.ROUND_HALF_UP).doubleValue();
		return f1;
	}
	public static void main(String args[]){
		double a = DateProcessing.get().decimalDigits(-0.777477775, 3);
		System.out.println(a);
	}
}
