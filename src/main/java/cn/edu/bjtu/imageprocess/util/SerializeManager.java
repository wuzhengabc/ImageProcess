package cn.edu.bjtu.imageprocess.util;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.commons.io.FileUtils;

/**
 * @author LMR
 *
 */
public class SerializeManager {

	private SerializeManager(){}
	private static SerializeManager s = new SerializeManager();
	public static SerializeManager getSerializeManager(){
		return s;
	}
	public void serializeToFile(Object o,String path){
		ObjectOutputStream objectOutputStream = null;
		try {
			objectOutputStream = new ObjectOutputStream(FileUtils.openOutputStream(new File(path)));
			objectOutputStream.writeObject(o);
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				objectOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	   
	}
	public Object deserializeFromFile(String path){
		try {
			ObjectInputStream oin = new ObjectInputStream(FileUtils.openInputStream(new File(path)));
			return oin.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
