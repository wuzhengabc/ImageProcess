package cn.edu.bjtu.imageprocess.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.commons.io.FileUtils;

import weka.core.Instances;

public class DataSetManager {
	private DataSetManager(){}
	private static final DataSetManager single = new DataSetManager();
	public static  DataSetManager get() {
		return single;
	}
	
	public Instances readInstances(String filepath,String filename){
		
		Instances insts = null;
		try {
			insts = new Instances(new FileReader(new File(filepath,filename)));
			insts.setClassIndex(insts.numAttributes()-1);//默认最后一个属性为类属性
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return insts;
	}
	public void toSerialize(Object o,String path){
		
		ObjectOutputStream objectOutputStream = null;
		try {
			objectOutputStream = new ObjectOutputStream(FileUtils.openOutputStream(new File(path)));
			objectOutputStream.writeObject(o);
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				objectOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	   
	}
	public Object toDeserialize(String path){
		try {
			ObjectInputStream oin = new ObjectInputStream(FileUtils.openInputStream(new File(path)));
			return oin.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
