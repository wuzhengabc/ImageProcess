package cn.edu.bjtu.imageprocess.algorithm;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;

/**
 * 这个类用于将时间序列数据集划分为具有和原数据集相同概率分布的训练集和测试集。
 * @author
 *
 */

public class DatasetSplitTraninAndTest {

    static double percentageTrain = 0.3;

    /**
     * simply loads the file on path or exits the program
     *
     * @param fullPath
     *          source path for ARFF file WITHOUT THE EXTENSION for some reason
     * @return Instances from path
     */
    public static Instances loadData(String fullPath) {

        // added by JAL, saves having to manually trim .arff from filename if it's
        // read in from somewhere/using old code
        if (fullPath.substring(fullPath.length() - 5, fullPath.length())
                .equalsIgnoreCase(".ARFF")) {
            fullPath = fullPath.substring(0, fullPath.length() - 5);
        }

        Instances d = null;
        // This class reads data in the stream by character
        FileReader r;
        try {
            r = new FileReader(fullPath + ".arff");
            d = new Instances(r);
            d.setClassIndex(d.numAttributes() - 1);
        } catch (IOException e) {
            System.out.println("Unable to load data on path " + fullPath
                    + " Exception thrown =" + e);
            System.exit(0);
        }
        return d;
    }

    /**
     * Simple util to saveDatasets out. Useful for shapelet transform.
     *
     * @param dataSet
     * @param fileName
     */
    public static void saveDataset(Instances dataSet, String fileName) {
        try {
            ArffSaver saver = new ArffSaver();
            saver.setInstances(dataSet);
            saver.setFile(new File(fileName + ".arff"));
            saver.writeBatch();
        } catch (IOException ex) {
            System.out.println("Error saving transformed dataset" + ex);
        }
    }

    public static ArrayList<Instances> splitTrainAndTest(Instances dataset) {

        ArrayList<Instances> trainAndTest = new ArrayList();

        int numInst = dataset.numInstances();

        int splitTrainN = (int)(numInst*percentageTrain);

        Instances splitTrain = new Instances(dataset, splitTrainN);

        Instances splitTest = new Instances(dataset, numInst - splitTrainN);

        TreeMap<Double,Integer> classDistribution = getClassDistributions(dataset);

        int[] trainClassInst = new int[dataset.numClasses()];

        int index = 0;

        for(Double d : classDistribution.keySet()) {
            trainClassInst[index] = (int)(classDistribution.get(d)*percentageTrain);
            index++;
        }

        int[] numk = new int[dataset.numClasses()];
        for(int i = 0; i < numInst; i++) {
            Instance inst = dataset.instance(i);
            int classIndex = (int) inst.classValue();
            numk[classIndex]++;
            if(numk[classIndex] < trainClassInst[classIndex]) {
                splitTrain.add(inst);
            }else {
                splitTest.add(inst);
            }

        }

        trainAndTest.add(splitTrain);
        trainAndTest.add(splitTest);

        return trainAndTest;

    }


    public Instances getSpiltTrain(ArrayList<Instances> data) {
        return data.get(0);
    }

    public Instances getSpiltTest(ArrayList<Instances> data) {
        return data.get(1);
    }


    /**
     *
     * @param data
     *            the input data set that the class distributions are to be
     *            derived from
     * @return a TreeMap<Double, Integer> in the form of <Class Value,
     *         Frequency>
     */
    private static TreeMap<Double, Integer> getClassDistributions(Instances data) {
        TreeMap<Double, Integer> classDistribution = new TreeMap<Double, Integer>();
        double classValue;
        for (int i = 0; i < data.numInstances(); i++) {
            classValue = data.instance(i).classValue();
            boolean classExists = false;
            // keySet(),all the classValue
            for (Double d : classDistribution.keySet()) {
                if (d == classValue) {
                    int temp = classDistribution.get(d);
                    // the frequency of the classValue
                    temp++;
                    classDistribution.put(classValue, temp);
                    // the old value is replaced
                    classExists = true;
                }
            }
            if (classExists == false) {
                classDistribution.put(classValue, 1);
            }
        }
        return classDistribution;
    }

    //public void action
    /**
     * 用于将时间序列数据集划分为训练集和测试集(具有和整个训练集相同的分布)
     * @param inputFilePath 输入文件路径
     * @param outputFilePath 输出文件路径
     * @param datasetName 输出数据集名字
     */
    public static void run(String inputFilePath, String outputFilePath,String datasetName) {
        Instances originData = loadData(inputFilePath + File.separator + datasetName + "_Standard");
        ArrayList<Instances> splitData = splitTrainAndTest(originData);
        Instances splitTrain = splitData.get(0);
        saveDataset(splitTrain, outputFilePath + datasetName + "_TRAIN");
        Instances splitTest = splitData.get(1);
        saveDataset(splitTest, outputFilePath + datasetName + "_TEST");
    }

}
