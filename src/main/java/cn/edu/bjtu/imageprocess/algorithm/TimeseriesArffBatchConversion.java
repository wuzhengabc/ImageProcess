/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.algorithm;

import cn.edu.bjtu.imageprocess.api.ValveChain;
import cn.edu.bjtu.imageprocess.core.StandardImagePipeline;
import cn.edu.bjtu.imageprocess.core.TimeSeresConverterValve.MyPoint;
import cn.edu.bjtu.imageprocess.image.ImageViewer;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.*;

/**
 * @author Alex
 *
 */
public class TimeseriesArffBatchConversion {

	// 图片名字
	String imageName = null;
	
	// 数据集名字
	String dataName = null;
	
	//protected ArrayList<Double> originOneList = new ArrayList();
	
	// 存放轮廓转换得到的原始数据
	protected ArrayList<ArrayList<Object>> originMatrix = new ArrayList();//no use
	
	// 采样点个数(包括类属性)
	static int numAtt = 100;

	/**
	 *  画出图像轮廓
	 * @param m 图像矩阵
	 * @param name 
	 */
	static void showImg(Mat m, String name) {
		ImageViewer iv = new ImageViewer(m, name);
		iv.imshow();
	}

	/**
	 * 将图片矩阵转换为轮廓点对象列表
	 * @param imageFilePath 图像存放路径
	 * @param imageName 图像名
	 * @return 轮廓上的点对象(角度，距离，横坐标，纵坐标)
	 */
	static List<MyPoint> singleTimeseriesConversion(String imageFilePath,String imageName) throws IOException
	{
		
		String imagePath = imageFilePath + imageName;
		//
		StandardImagePipeline pp = new StandardImagePipeline();
		//
		ValveChain vc = pp.getValveChain();
		//
		vc.setConfigurationKeyWithValue("gray.threshold", "0.5").setConfigurationKeyWithValue("scale.ratio", "0.2");
		//
		Map<String, String> map = vc.getConfigurationMap();
		System.out.println(map);

		String imgType = "jpg";
		if ((imageName != null) && (imageName.length() > 0)) {
			int dot = imageName.lastIndexOf('.');
			if ((dot >-1) && (dot < (imageName.length() - 1))) {
				imgType = imageName.substring(dot + 1).toLowerCase();
			}
		}
		File f= new File(imagePath);

		if(imgType.equals("gif"))
		{
			BufferedImage src = javax.imageio.ImageIO.read(f);
			int width = src.getWidth(null);
			int height = src.getHeight(null);

			BufferedImage tag = new BufferedImage(width , height ,BufferedImage.TYPE_INT_RGB);
			tag.getGraphics().drawImage(src, 0, 0, width , height , null);
			imageName = imageName.substring(0,imageName.lastIndexOf('.')) + ".jpg";
			FileOutputStream out = new FileOutputStream(imageFilePath + "/" + imageName);
			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
			encoder.encode(tag);
			out.close();
			f= new File(imageFilePath , imageName);
		}

		Mat src = Imgcodecs.imread(f.getAbsolutePath());
		pp.setInput(src);
		pp.action();
		Mat res = pp.getFinalResult();
		// showImg(res, "canny img");
		Object[] listArr = pp.getFinalOtherResult();
		List<MyPoint> l = (List<MyPoint>) listArr[0];
		File file = new File(f.getAbsolutePath());
		if (file.exists() && file.isFile())
			file.delete();
		return l;
	}
	

	/**
	 * 将一张图片转换为时间序列
	 * @param imageFP 图片的存放路径
	 * @param name  图片名字
	 * @return 返回轮廓上点对象距离组成的动态数组
	 * @throws Exception
	 */
	public ArrayList singleTSConversion(String imageFP, String name) throws Exception {
		
       List<MyPoint> l = singleTimeseriesConversion(imageFP, name);
		// output into txt
//		String logFilePath = outputPath;
//		String fileName = "TimeseriesConversion.txt";
//		String logFile = logFilePath + fileName;
       
       ArrayList<Object> oneOriginTS = new ArrayList();
       for(int i = 0; i < l.size(); i++) {
    	   oneOriginTS.add(l.get(i).distance);
       }
       
       // 获得图片文件名作为时间序列的类属性值
       String classValue = name.substring(0, name.lastIndexOf("-"));
       oneOriginTS.add(classValue);

       return oneOriginTS;
	}
	
	/**
	 * 将指定文件夹下的图像都转换为时间序列
	 * @param imageFP 图像存储路径
	 * @return 返回时间序列组成的动态数组(可能不等长)
	 * @throws Exception
	 */
	public ArrayList<ArrayList<Object>> batchConversion(String imageFP) throws Exception {
		
		ArrayList<ArrayList<Object>> oM = new ArrayList();
		ArrayList oneT = new ArrayList();
		
		//获得图像名数组
		String[] names = getImagesNames(imageFP);
		for(int i = 0; i < names.length; i++) {
			String name = names[i];
			oneT = singleTSConversion(imageFP, name);
			oM.add(oneT);
		}
		return oM;
	}
	
	/**
	 * 读取指定文件夹下的所有图片的文件名(包含图像类属性和序号信息(不重要))
	 * @param filePath 图像文件存放路径
	 * @return 返回文件名数组
	 */
	private String[] getImagesNames(String filePath){
		String[] imageNameList = null;
		
		File file = new File(filePath);
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				File currFile = new File(dir, name);
				if(currFile.isFile() && (name.endsWith(".jpg")||name.endsWith(".BMP")||name.endsWith(".png")||
						name.endsWith(".jpeg")||name.endsWith(".gif"))) {
					return true;
				}else {
					return false;
				}
			}

		};
		
		if(file.exists()) {
			imageNameList = file.list(filter);			
		}				
		
		return imageNameList;
	}
	
	/**
	 * 获得存放时间序列originM矩阵中的最短长度
	 * @param originM 存放时间序列的矩阵
	 * @return 返回originM矩阵中的最短长度
	 */
	public int getNumAttributes(ArrayList<ArrayList<Object>> originM) {
		int minLength = Integer.MAX_VALUE;
		int numInsts =  originM.size();
		for(int i = 0; i < numInsts; i++) {
			int tsLength = originM.get(i).size();
			if(tsLength < minLength) {
				minLength = tsLength;
			}
		}		
		
		return minLength;
	}
	
	/**
	 * 获得类属性取值集合
	 * @param originM 时间序列数据存放矩阵
	 * @return 时间序列属性取值集合
	 */
	public TreeSet getClassValueSet(ArrayList<ArrayList<Object>> originM) {
		TreeSet classValueSet = new TreeSet();
		String classV = null;
		for(int i = 0; i < originM.size(); i++) {
			int length = originM.get(i).size();
			classV = (String)originM.get(i).get(length-1);
			classValueSet.add(classV);
		}
						
		return classValueSet;
	}
	
	/**
	 * 时间序列矩阵转换输出arff格式文件
	 * @param originM 时间序列数据存放矩阵
	 * @param outputFile 输出文件地址路径
	 * @throws Exception
	 */
	public void timeseriesArffSaver(ArrayList<ArrayList<Object>> originM,File outputFile) throws Exception {
		int numA = getNumAttributes(originM);
		ArrayList<Attribute> atts = new ArrayList<>();
		String name;
		for(int j = 0; j < numA - 1; j++) {
			name = "Att_" + j;
			atts.add(new Attribute(name));
		}
		
		// Get the class value as a ArrayList
		TreeSet classValueSet = getClassValueSet(originM);
		ArrayList<String> vals = new ArrayList<>();
		Iterator<String> cIt = classValueSet.iterator();
		while(cIt.hasNext()) {
			vals.add(cIt.next());
		}
		
		atts.add(new Attribute("target",vals));
		
		Instances result = new Instances("TS",atts, originM.size());
		result.setClassIndex(numA-1);
		
		for(int i = 0; i < originM.size(); i++) {
			DenseInstance in = new DenseInstance(numA);
			int lastIndex = originM.get(i).size() - 1;
			for(int j = 0; j < in.numAttributes() - 1; j++) {
				in.setValue(j, (double)originM.get(i).get(j));
			}
			in.setDataset(result);
			in.setClassValue(String.valueOf(originM.get(i).get(lastIndex)));
			//in.setClValue(in.numAttributes()-1, String.valueOf(originM.get(i).get(lastIndex)));
			
			// System.out.println(in);

			result.add(in);
		}

		ArffSaver aS = new ArffSaver();
		aS.setFile(outputFile);
		aS.setInstances(result);
		aS.writeBatch();
	}

	/**
	 *  将转换得到的不等长数据转换为等长数据
	 * @param originM 原始转换得到的时间序列矩阵
	 * @param numA 转换后属性取值个数(不包括类属性)
	 * @return
	 */
	public ArrayList<ArrayList<Object>> equilongMatTransform(ArrayList<ArrayList<Object>> originM, int numA) {
		ArrayList<ArrayList<Object>> equilongM = new ArrayList();
		
		int minL = getNumAttributes(originM);
		
		// 采样点个数限制不超过原时间序列最短长度
		if(numA > minL) {
			numA = minL;
		}
		
		int sizeM = originM.size();
		for(int i = 0; i < sizeM; i++) {
			ArrayList equilongTS = equilongSamp1TS(originM.get(i), numA);
			equilongM.add(equilongTS);
		}
		
		return equilongM;
	}
	

	/**
	 * 将原始时间转换成指定长度的时间序列数据,取前numA个值
	 * @param originTS 原时间序列
	 * @param numA 转换后时间序列属性个数
	 * @return
	 */
	public ArrayList equilongTS(ArrayList originTS, int numA) {
		ArrayList equilongTS = new ArrayList();
		
		int l = originTS.size();
				
		//
		for(int i = 0; i < numA - 1; i++) {
			equilongTS.add(originTS.get(i));
		}
		
		equilongTS.add(originTS.get(l-1));
		
		return equilongTS;
	}
	
	// 对轮廓上的点进行等距采样，起点为0
	/**
	 * 通过等间隔采样取numA个原时间序列的值组成新时间序列
	 * @param originTS 原时间序列
	 * @param numA 转换后属性取值个数
	 * @return
	 */
	public ArrayList equilongSamp1TS(ArrayList originTS, int numA) {
		ArrayList equilongTS = new ArrayList();
		
		if(originTS.size() <= numA) {
			equilongTS = originTS;
		}else {
			int stepL = originTS.size()/numA;
			equilongTS.add(originTS.get(0));
			for(int i = 0; i < numA - 1; i++) {
				equilongTS.add(originTS.get(i + stepL));
			}
			equilongTS.add(originTS.get(originTS.size()-1));
		}
		return equilongTS;
	}

	/**
	 * 转换
	 * @param imageFilePath 图像文件存储路径
	 * @param logFilePath 转换得到的时间序列存储路径
	 */
	public static void run(String imageFilePath, String logFilePath, String datasetName) {

		// 转换得到arff格式文件存放路径和文件名
		String logFile = logFilePath + datasetName + ".arff";
		
		TimeseriesArffBatchConversion tbc = new TimeseriesArffBatchConversion();
		ArrayList<ArrayList<Object>> originData = new ArrayList();
		try
		{
			// 原时间序列组成的矩阵
			originData = tbc.batchConversion(imageFilePath);
			// 等长时间序列的属性个数
			numAtt = 100;

			// 等长时间序列存放矩阵
			ArrayList<ArrayList<Object>> equilongData = new ArrayList();
			equilongData = tbc.equilongMatTransform(originData, numAtt);

			// arff格式文件输出
			File file = new File(logFile);
			tbc.timeseriesArffSaver(equilongData, file);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
