package cn.edu.bjtu.imageprocess.algorithm;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Standardize;

public class StandardizeTransform {

    Instances m_Instances = null;

    /**
     * simply loads the file on path or exits the program
     *
     * @param fullPath
     *          source path for ARFF file WITHOUT THE EXTENSION for some reason
     * @return Instances from path
     */
    public static Instances loadData(String fullPath) {

        // added by JAL, saves having to manually trim .arff from filename if it's
        // read in from somewhere/using old code
        if (fullPath.substring(fullPath.length() - 5, fullPath.length())
                .equalsIgnoreCase(".ARFF")) {
            fullPath = fullPath.substring(0, fullPath.length() - 5);
        }

        Instances d = null;
        // This class reads data in the stream by character
        FileReader r;
        try {
            r = new FileReader(fullPath + ".arff");
            d = new Instances(r);
            d.setClassIndex(d.numAttributes() - 1);
        } catch (IOException e) {
            System.out.println("Unable to load data on path " + fullPath
                    + " Exception thrown =" + e);
            System.exit(0);
        }
        return d;
    }

    /**
     * Simple util to saveDatasets out. Useful for shapelet transform.
     *
     * @param dataSet
     * @param fileName
     */
    public static void saveDataset(Instances dataSet, String fileName) {
        try {
            ArffSaver saver = new ArffSaver();
            saver.setInstances(dataSet);
            saver.setFile(new File(fileName + ".arff"));
            saver.writeBatch();
        } catch (IOException ex) {
            System.out.println("Error saving transformed dataset" + ex);
        }
    }

    public static double variance(Instance inst) {
        double instVariance = 0;

        double sum = 0;

        double instMean = average(inst);

        double attWeights = 0;

        int length = inst.numAttributes();

        for(int i = 0; i < length -1; i++) {

            double attValue = inst.value(i)*inst.attribute(i).weight();

            attWeights += inst.attribute(i).weight();

            sum += (attValue-instMean)*(attValue-instMean);

        }

        instVariance = Math.sqrt(sum/(attWeights - 1));

        return instVariance;

    }

    public static double average(Instance inst) {

        double instAverage = 0;

        double sum  = 0;
        double sumWeights = 0;
        int length = inst.numAttributes();

        for(int i = 0; i < length -1; i++) {

            double attValue = inst.value(i)*inst.attribute(i).weight();

            sumWeights += inst.attribute(i).weight();

            sum += attValue;

        }

        instAverage = sum/sumWeights;

        return instAverage;

    }

    public static Instances runStandardTransform(Instances insts) {

        Instances standardData = new Instances(insts, insts.size());

        int numInst = insts.size();

        int numAtt = insts.numAttributes();

        for(int i = 0; i < numInst; i++) {

            Instance instance = insts.instance(i);

            DenseInstance standardInst = new DenseInstance(numAtt);

            double var = variance(instance);

            double mean = average(instance);

            if(var <= 0) {

                standardInst = new DenseInstance(instance);

            }else {

                for(int j = 0; j < numAtt - 1; j++) {

                    standardInst.setValue(j, (instance.value(j) - mean)/var);
                }

            }

            standardInst.setDataset(standardData);

            standardInst.setClassValue(instance.classValue());

            System.out.println(standardInst.classValue());

            standardData.add(standardInst);

        }



        return standardData;


    }

    public static void run(String originDataLocation, String standardDataPath, String datasetName){
        // TODO Auto-generated method stub

        Instances originInsts = loadData(originDataLocation + File.separator + datasetName);

        Instances standardData = null;

        standardData = runStandardTransform(originInsts);

        saveDataset(standardData, standardDataPath  + datasetName + "_Standard");
    }

}

