package cn.edu.bjtu.imageprocess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.EmbeddedServletContainer;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import cn.edu.bjtu.imageprocess.config.AppConfig;

import java.io.*;
import java.util.Properties;


@SpringBootApplication
	@EnableJpaRepositories("cn.edu.bjtu.imageprocess.persistence.dao")
	@EntityScan("cn.edu.bjtu.imageprocess.persistence.entity")
	@Import(AppConfig.class)
	public class App extends TomcatEmbeddedServletContainerFactory{
	public static void main(String[] args) {
		String classesPath = new App().getClass().getResource("/").getPath();
		String propFilePath = "configuration/file.properties";
		String userDir = System.getProperty("user.dir");

		String arffDir = "/src/main/webapp/timeseries/data";
		String imgDir = "/src/main/webapp/timeseries/img";
		String resultImgDir = "/src/main/webapp/timeseries/result";
		String classDir ="/src/main/webapp/timeseries/classifiers";
		FileInputStream in = null;
		FileOutputStream out = null;
		Properties properties = new Properties();
		try
		{
			in = new FileInputStream(classesPath + propFilePath);
			properties.load(in);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		properties.setProperty("arffDir", userDir + arffDir);
		properties.setProperty("imgDir", userDir + imgDir);
		properties.setProperty("resultImgDir", userDir + resultImgDir);
		properties.setProperty("classDir", userDir + classDir);
		
		try
		{
			out = new FileOutputStream(classesPath + propFilePath);
			properties.store(out,null);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		SpringApplication.run(App.class, args);
    }

	@Override
	public EmbeddedServletContainer getEmbeddedServletContainer(ServletContextInitializer... initializers) {
		return super.getEmbeddedServletContainer(initializers);
	}
}

//public class App extends SpringBootServletInitializer
//{
//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		return application.sources(App.class);
//	}
//
//	public static void main(String[] args) throws Exception {
//		SpringApplication.run(App.class, args);
//	}
//}