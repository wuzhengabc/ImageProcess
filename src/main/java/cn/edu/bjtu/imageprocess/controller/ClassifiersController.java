package cn.edu.bjtu.imageprocess.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import cn.edu.bjtu.imageprocess.config.FileConfiguration;
import cn.edu.bjtu.imageprocess.dto.ClassifiersFile;
import cn.edu.bjtu.imageprocess.dto.OtherEvaItem;
import cn.edu.bjtu.imageprocess.dto.RocItem;
import cn.edu.bjtu.imageprocess.util.DataSetManager;
import cn.edu.bjtu.imageprocess.util.SerializeManager;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.evaluation.ThresholdCurve;
import weka.classifiers.lazy.IBk;
import weka.core.Instances;
/**
 * @author LMR
 *
 */
@RestController
@RequestMapping("/tsc")
public class ClassifiersController {

	@Autowired FileConfiguration fc;
	@RequestMapping("/classifiers/files")
	@ResponseBody
	Object showClassifiersFiles() {
		System.out.println(fc.getClassDir()+"---");
		File f = new File(fc.getClassDir());
		//System.out.println(f==null);
		//return new ClassifiersDto()
		return Arrays.asList(f.listFiles()).stream().map((x)->new ClassifiersFile(x.getName(),x.getName().substring(0, x.getName().indexOf("_")))).collect(Collectors.toList());
	}
	
	@RequestMapping("/create/{name:.+}")
	Object createClassifier(@PathVariable String name,HttpServletRequest req){
		System.out.println(name+"************");
		String algo = req.getParameter("algo");
		//List<String> mes = Arrays.asList(name.split("-"));
		Map<String,String> content = new HashMap<String,String>();
		try {
			File f = FileUtils.getFile(fc.getClassDir(),algo+"_"+name.substring(0, name.lastIndexOf("."))+".classifier");
			//System.out.println(f.exists());
			if(f.exists())
			{
				content.put("error","error1");
			}
			else{
				Instances insts = DataSetManager.get().readInstances(fc.getArffDir(), name);
				//NaiveBayes classifier = new NaiveBayes();
				//classifier.buildClassifier(insts);
				String ccname = algo.trim();
				String pkgName= "weka.classifiers.bayes.";
				Class<AbstractClassifier> cnameOfClass = null;
				try{
					System.out.println(pkgName+ccname);
					   cnameOfClass = (Class<AbstractClassifier>) Class.forName(pkgName+ccname);	
					
				}catch(ClassNotFoundException e){
					String newname = "weka.classifiers.lazy."+ccname;
					cnameOfClass = (Class<AbstractClassifier>) Class.forName(newname);	
				}
				//
				/*ServiceLoader<AbstractClassifier> sloader = ServiceLoader.load(AbstractClassifier.class);
				sloader.forEach(System.out::println);*/
				AbstractClassifier cb = cnameOfClass.newInstance();
//				AbstractClassifier cb2 = cnameOfClass.newInstance();
				if(cb instanceof IBk)
				{
					//if (cb instanceof kNN)
				}
				if(cb instanceof NaiveBayes){
					cb.buildClassifier(insts);	
				}else {
					//((IBk)cb).setKNN(10);
					((IBk)cb).setKNN(10);
					((IBk)cb).buildClassifier(insts);
				}
				//(KNN)cb
				//cb.buildClassifier(arg0);
				String path = fc.getClassDir()+"/"+algo+"_"+name.substring(0, name.lastIndexOf("."))+".classifier";
				SerializeManager.getSerializeManager().serializeToFile(cb, path);
				//DataSetManager.get().toSerialize(cb,path);
				/*String[] options = {"-t",new File(fc.getArffDir(),mes.get(1)).getAbsolutePath()};
				String a =Evaluation.evaluateModel(new NaiveBayes(), options);
				String result[] = a.split("\n");
				for(int i = 0;i<result.length;i++)
				{	
					if(i<10)
						content.put("0"+i, result[i].replace(" ", "..."));
					else
						content.put(""+i, result[i].replace(" ", "..."));
					}*/
				content.put("error","none");
			}
			return content;
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	
		return "bb";
		
	}

	@RequestMapping("/testClassifiers/{name:.+}")
	@ResponseBody
	Object testClassifiers(@PathVariable String name,HttpServletRequest res){
		String classifier = res.getParameter("classifier");
		Map<String,List> eva = new HashMap<String,List>();
		try {
			Instances insts = DataSetManager.get().readInstances(fc.getArffDir(), name);
			String path = fc.getClassDir()+"/"+classifier;
			Classifier c = (Classifier)SerializeManager.getSerializeManager().deserializeFromFile(path);
			Evaluation eval  = new Evaluation(insts);
			//eval.evaluateModel(c, insts);
			eval.crossValidateModel(c, insts, 5, new Random(1));
			ThresholdCurve tc = new ThresholdCurve();
			int classIndex = 0;			//classIndex 是 "positive"，默认第一个
			Instances result = tc.getCurve(eval.predictions(), classIndex);
			int tpIndex = result.attribute(ThresholdCurve.TP_RATE_NAME).index();
			int fpIndex = result.attribute(ThresholdCurve.FP_RATE_NAME).index();
			double [] tpRate = result.attributeToDoubleArray(tpIndex);
			double [] fpRate = result.attributeToDoubleArray(fpIndex);
			List<RocItem> roc = new ArrayList<RocItem>();
			List<OtherEvaItem> other = new ArrayList<OtherEvaItem>();
			for(int i = tpRate.length-1;i>=0;i--)
			{
				roc.add(new RocItem(tpRate[i],fpRate[i]));
			}
			other.add(new OtherEvaItem(eval.kappa(),eval.meanAbsoluteError(),eval.precision(classIndex),eval.recall(classIndex),eval.areaUnderROC(classIndex),eval.correct(),eval.incorrect()));
			eva.put("roc", roc);
			eva.put("other", other);
			return eva;
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("/deleteClassifiers/{name:.+}")
	@ResponseBody
	void deleteClassifier(@PathVariable String name){
		try{
			File f= new File(fc.getClassDir() , name);
			File file = new File(f.getAbsolutePath());
			if (file.exists() && file.isFile())
				file.delete();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
