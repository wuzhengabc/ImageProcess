package cn.edu.bjtu.imageprocess.controller;

import lombok.Data;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.edu.bjtu.imageprocess.config.FileConfiguration;
import cn.edu.bjtu.imageprocess.core.LoggerSupport;
import cn.edu.bjtu.imageprocess.persistence.service.CAService;

@RestController
public class HelloController extends LoggerSupport{
	
    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    CAService ca;
    @Autowired
    FileConfiguration cf;
    @RequestMapping("/")
    String hello() {
        return "Hello World!";
    }
    @RequestMapping("/testDao")
    Object cg(@RequestParam boolean flag,  HttpServletRequest req,HttpServletResponse res){
    	ca.method(flag);
    	return "OK";
    }
    //最大100个连接,最大50个线程,所以连接数多于线程数
    @RequestMapping("/ab/async")
    void ab(HttpServletRequest req,HttpServletResponse res) throws Exception{
    	System.out.println(cf.getAlex());
    	res.getWriter().println("ok");
    }
    @RequestMapping("/ab/sync")
    void syncab(HttpServletRequest req,HttpServletResponse res) throws Exception{
    }
    @Data
    static class Result {
        private final int left;
        private final int right;
        private final long answer;
		public Result(int left,int right,long answ) {
			this.left = left;
			this.right = right;
			this.answer = answ;
		}
		public int getLeft() {
			return left;
		}
		public int getRight() {
			return right;
		}
		public long getAnswer() {
			return answer;
		}
		
       
    }
    // SQL sample
    @RequestMapping("calc")
    Result calc(@RequestParam int left, @RequestParam int right) {
        MapSqlParameterSource source = new MapSqlParameterSource()
                .addValue("left", left)
                .addValue("right", right);
        return jdbcTemplate.queryForObject("SELECT :left + :right AS answer", source,
                (rs, rowNum) -> new Result(left, right, rs.getLong("answer")));
    }
}
