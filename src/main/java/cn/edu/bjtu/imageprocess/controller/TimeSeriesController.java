/**
 * 2017年8月18日
 */
package cn.edu.bjtu.imageprocess.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.edu.bjtu.imageprocess.algorithm.*;
import cn.edu.bjtu.imageprocess.api.ValveChain;
import cn.edu.bjtu.imageprocess.algorithm.TimeseriesArffBatchConversion;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import org.apache.commons.io.FileUtils;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;

import org.opencv.imgproc.Imgproc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.edu.bjtu.imageprocess.config.FileConfiguration;
import cn.edu.bjtu.imageprocess.core.StandardImagePipeline;
import cn.edu.bjtu.imageprocess.dto.ArffListDto;
import cn.edu.bjtu.imageprocess.dto.ClassifiersFile;
import cn.edu.bjtu.imageprocess.dto.FileItem;
import cn.edu.bjtu.imageprocess.dto.OtherEvaItem;
import cn.edu.bjtu.imageprocess.dto.RocItem;
import cn.edu.bjtu.imageprocess.util.DataSetManager;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.evaluation.ThresholdCurve;
import weka.classifiers.lazy.IBk;
import weka.core.Instances;
import weka.core.converters.JSONSaver;

/**
 * @author Alex
 *
 */
@RestController
@RequestMapping("/tsc")
public class TimeSeriesController {
	static Map<String, String> contentType = new HashMap<>();
	static{

		contentType.put("jpg", "image/jpeg;charset=utf-8");
		contentType.put("jpeg", "image/jpeg;charset=utf-8");
		contentType.put("gif", "image/gif;charset=utf-8");
		contentType.put("bmp", "image/bmp;charset=utf-8");
		contentType.put("tiff", "image/tiff;charset=utf-8");
		contentType.put("png", "image/png;charset=utf-8");
		String osName = System.getProperty("os.name");
		String osArch = System.getProperty("os.arch");
		String userDir = System.getProperty("user.dir");
		System.out.println(osName);
		if(osName.equals("Mac OS X"))
			System.load(userDir + "/src/main/lib/osx/libopencv_java330.dylib");
		if(osName.equals("Linux"))
			System.load(userDir + "/src/main/lib/linux/libopencv_java330.so");
		if((osName.contains("Windows") && osArch.equals("x86")))
			System.load(userDir + "/src/main/lib/win/x86/opencv_java330.dll");
		if((osName.contains("Windows") && osArch.equals("amd64")))
			System.load(userDir + "/src/main/lib/win/x64/opencv_java330.dll");
	}
	@Autowired FileConfiguration fc;
	@Autowired ThreadLocal<JSONSaver> jsonSavers;
	@Autowired ThreadLocal<StandardImagePipeline> pipelines;
	@RequestMapping("/files")
	@ResponseBody
	Object showFiles() {
		File f = new File(fc.getArffDir());
		return new ArffListDto(Arrays.asList(f.listFiles()).stream().map((x)->new FileItem(x.getName(),x.length())).collect(Collectors.toList()));
	}
	@RequestMapping("/file/{name:.+}")
	void showFile(@PathVariable String name,HttpServletResponse res) {
		try{
			Instances insts = new Instances(new FileReader(new File(fc.getArffDir(),name)));
			JSONSaver js = jsonSavers.get();
			js.setInstances(insts);
			js.setDestination(res.getOutputStream());
			js.writeBatch();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	@RequestMapping("/delete/{name:.+}")
	void deleteFile(@PathVariable String name,HttpServletResponse res) {
		try{
			File f= new File(fc.getArffDir() , name);
			File file = new File(f.getAbsolutePath());
			if (file.exists() && file.isFile())
				file.delete();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
//  如果	/img/{name} /img/032.jpg name只能匹配到032 ,不包括jpg 上面同理,这里修正
	@RequestMapping("/img/{name:.+}")
	void showImg(@PathVariable String name,HttpServletResponse res) {
		try{
			String imgType = "jpg";
			if ((name != null) && (name.length() > 0)) {
				int dot = name.lastIndexOf('.');
				if ((dot >-1) && (dot < (name.length() - 1))) {
					imgType = name.substring(dot + 1);
				}
			}
			res.setContentType(contentType.get(imgType));
			File f= new File(fc.getImgDir() , name);
			FileUtils.copyFile(f,res.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@RequestMapping("/img/files")
	@ResponseBody
	Object showImgFiles() {
		File f = new File(fc.getImgDir());
		return new ArffListDto(Arrays.asList(f.listFiles()).stream().map((x)->new FileItem(x.getName(),x.length())).collect(Collectors.toList()));
	}

	@RequestMapping("/img/serialize")
	@ResponseBody
	Object pipelineProcessImage(HttpServletRequest req,HttpServletResponse res ,@RequestParam String name) {
		try{
			String imgType = "jpg";
			if ((name != null) && (name.length() > 0)) {
				int dot = name.lastIndexOf('.');
				if ((dot >-1) && (dot < (name.length() - 1))) {
					imgType = name.substring(dot + 1).toLowerCase();
				}
			}
			File f= new File(fc.getImgDir() , name);
			res.setContentType(contentType.get(imgType));

			if(imgType.equals("gif"))
			{
				BufferedImage src = javax.imageio.ImageIO.read(f);
				int width = src.getWidth(null);
				int height = src.getHeight(null);

				BufferedImage tag = new BufferedImage(width , height ,BufferedImage.TYPE_INT_RGB);
				tag.getGraphics().drawImage(src, 0, 0, width , height , null);
				name = name.substring(0,name.lastIndexOf('.')) + ".jpg";
				FileOutputStream out = new FileOutputStream(fc.getImgDir() + "/" + name);
				JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
				encoder.encode(tag);
				out.close();
				f= new File(fc.getImgDir() , name);
			}

			StandardImagePipeline sipp = pipelines.get();
			ValveChain vc = sipp.getValveChain();
            String blurSize = req.getParameter("blursize");
            String sigmaX = req.getParameter("sigmax");
			String thresholdThresh = req.getParameter("thresholdthresh");
			String thresholdMaxVal = req.getParameter("thresholdmaxVal");
			String cannyThreshold1 = req.getParameter("cannythreshold1");
			String cannyThreshold2 = req.getParameter("cannythreshold2");

			//设置参数
			vc.setConfigurationKeyWithValue("gray.threshold", "0.5")
			.setConfigurationKeyWithValue("gaussianblur.sigmaX", sigmaX)
			.setConfigurationKeyWithValue("scale.ratio", "0.2")
			.setConfigurationKeyWithValue("gaussianblur.size.width", blurSize)
			.setConfigurationKeyWithValue("gaussianblur.size.height", blurSize)
			.setConfigurationKeyWithValue("threshold.thresh", thresholdThresh)
			.setConfigurationKeyWithValue("threshold.maxVal", thresholdMaxVal)
			.setConfigurationKeyWithValue("canny.threshold1", cannyThreshold1)
			.setConfigurationKeyWithValue("canny.threshold2", cannyThreshold2);

			Mat m = Imgcodecs.imread(f.getAbsolutePath());
			sipp.setInput(m);
			sipp.action();
			Object [] result = sipp.getFinalOtherResult();
			List l = (List)result[0]; //里面是转换成的MyPoint 
			Point p = (Point)result[1]; //质心位置
			List<Mat> allResults = sipp.getAllResults();
			String resultImgDir = fc.getResultImgDir();
			String[] processImg = {"Original","Gray","Blur","Threshold","Canny","Centroid","TimeSeres"};
			Imgproc.drawMarker(allResults.get(4), p, new Scalar(255));
			for(int i = 0; i < allResults.size(); i++)
			{
				Imgcodecs.imwrite(resultImgDir + "/" + processImg[i] + name, allResults.get(i));
			}

			File file = new File(f.getAbsolutePath());
			if (file.exists() && file.isFile())
				file.delete();

			return l;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	String datasetName = "default";

	@RequestMapping("/img/batchconvert")
	@ResponseBody
	Object batchConvert(HttpServletRequest req) {
		datasetName = req.getParameter("datasetname");
		TimeseriesArffBatchConversion.run(fc.getImgDir() + "/",fc.getArffDir() + "/", datasetName);
		return null;
	}

	@RequestMapping("/img/normalization")
	@ResponseBody
	Object normalization() {
		StandardizeTransform.run(fc.getArffDir() + "/",fc.getArffDir() + "/", datasetName);
		return null;
	}

	@RequestMapping("/img/splitdataset")
	@ResponseBody
	Object splitDataSet() {
		DatasetSplitTraninAndTest.run(fc.getArffDir() + "/",fc.getArffDir() + "/", datasetName);
		return null;
	}

	
}
