/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.core;

import java.util.Map;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import cn.edu.bjtu.imageprocess.annotation.Stateless;
import cn.edu.bjtu.imageprocess.api.Valve;
import cn.edu.bjtu.imageprocess.api.ValveChain;

/**
 * @author Alex
 *
 */
@Stateless
public class ThresholdValve implements Valve {
	@Override
	public void transform(Mat src, ValveChain vc,Object ...otherArgs) {
		Map<String,String> config = vc.getConfigurationMap();
		double thresh = Double.valueOf(config.getOrDefault("threshold.thresh","200"));
		double maxVal = Double.valueOf(config.getOrDefault("threshold.maxVal", "255"));
		try{
			Mat m = Mat.zeros(src.size(), src.type());
			Imgproc.threshold(src, m, thresh, maxVal, Imgproc.THRESH_BINARY);
			vc.transformNext(m);
		}catch(Exception e){
			vc.handleException(e, src);
		}
	}

}
