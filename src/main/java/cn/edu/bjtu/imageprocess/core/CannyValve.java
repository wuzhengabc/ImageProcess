/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.core;

import java.util.Map;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import cn.edu.bjtu.imageprocess.annotation.Stateless;
import cn.edu.bjtu.imageprocess.api.Valve;
import cn.edu.bjtu.imageprocess.api.ValveChain;

/**
 * @author Alex
 *
 */
@Stateless
public class CannyValve implements Valve{
	@Override
	public void transform(Mat src, ValveChain vc,Object ...otherArgs) {
		Map<String,String> config = vc.getConfigurationMap();
		double thresh1 = Double.valueOf(config.getOrDefault("canny.threshold1","0.7"));
		double thresh2 = Double.valueOf(config.getOrDefault("canny.threshold2","0.7"));
		try{
			Mat res = Mat.zeros(src.size(), src.type());
			Imgproc.Canny(src, res, thresh1, thresh2);
			vc.transformNext(res);
		}catch(Exception e){
			vc.handleException(e, src);
		}
	}
	

}
