/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.core;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import cn.edu.bjtu.imageprocess.annotation.Stateless;
import cn.edu.bjtu.imageprocess.api.Valve;
import cn.edu.bjtu.imageprocess.api.ValveChain;

/**
 * @author Alex
 *
 */
@Stateless
public class CentroidValve implements Valve {

	@Override
	public void transform(Mat src, ValveChain vc, Object... otherArgs) {
		Moments ms = Imgproc.moments(src);
		Point p = new Point(ms.m10/ms.m00,ms.m01/ms.m00);
		vc.transformNext(src,p);
	}

}
