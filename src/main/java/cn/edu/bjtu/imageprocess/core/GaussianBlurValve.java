/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.core;

import java.util.Map;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import cn.edu.bjtu.imageprocess.annotation.Stateless;
import cn.edu.bjtu.imageprocess.api.Valve;
import cn.edu.bjtu.imageprocess.api.ValveChain;

/**
 * @author Alex
 *
 */
@Stateless
public class GaussianBlurValve implements Valve {

	@Override
	public void transform(Mat src, ValveChain vc,Object ...otherArgs) {
		Map<String,String>map = vc.getConfigurationMap();
		double width = Double.valueOf(map.getOrDefault("gaussianblur.size.width", "15"));
		double height = Double.valueOf(map.getOrDefault("gaussianblur.size.height", "15"));
		double sigmaX = Double.valueOf(map.getOrDefault("gaussianblur.sigmaX", "0.5"));
		Mat res = Mat.zeros(src.size(), src.type());
		Imgproc.GaussianBlur(src, res, new Size(width,height), sigmaX);
		vc.transformNext(res);
	}

}
