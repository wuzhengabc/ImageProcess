/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.core;

import org.opencv.core.Mat;

import cn.edu.bjtu.imageprocess.annotation.Stateless;
import cn.edu.bjtu.imageprocess.api.Valve;
import cn.edu.bjtu.imageprocess.api.ValveChain;

/**
 * 缩放
 * @author Alex
 *
 */
@Stateless
public class ScaleValve implements Valve {

	public void transform(Mat src, ValveChain vc,Object ...otherArgs) {
		vc.transformNext(src);
	}

}
