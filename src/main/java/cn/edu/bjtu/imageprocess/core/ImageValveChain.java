/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opencv.core.Mat;

import cn.edu.bjtu.imageprocess.annotation.Stateful;
import cn.edu.bjtu.imageprocess.api.Valve;
import cn.edu.bjtu.imageprocess.api.ValveChain;

/**
 * 
 * Valve的设计是无状态的
 * ValveChain是有状态的,保存了Vavlue的转换的中间结果
 * @author Alex
 */
@Stateful
public class ImageValveChain implements ValveChain {
	private ArrayList<Valve> valves = new ArrayList<Valve>(10);
	private ArrayList<Mat>   results = new ArrayList<Mat>(10);
	private ArrayList<Object[]> otherResults = new ArrayList<Object[]>(10);
	private Map<String,String> configurationMap = new HashMap<String,String>();
	/**
	 * 当前要进行转换的Valve的序号
	 */
	private int currentIndex = -1;
	/**
	 * 保存当前Valve的输入结果,也是上一个Valve的输出结果.
	 */
	private Mat currentResult = null;
	public ImageValveChain(Valve ... vals) {
		for (Valve v : vals){
			valves.add(v);
		}
	}
	@Override
	public void transformNext(Mat src,Object ... otherArgs) {
		currentResult = src;
		results.add(src);
		otherResults.add(otherArgs);
		if(currentIndex<valves.size()-1){
			currentIndex ++ ;
			valves.get(currentIndex).transform(src, this ,otherArgs);
		}
	}
	public Mat getResult(){
		return currentResult;
	}
	@Override
	public void reset() {
		currentIndex = -1;
		results.clear();
		otherResults.clear();
		configurationMap.clear();
	}
	@Override
	public void handleException(Exception e ,Mat m) {
		transformTerminal(m);
		e.printStackTrace();
	}
	@Override
	public Mat getOutput() {
		return currentResult;
	}
	@Override
	public void transformTerminal(Mat src ,Object ... otherArgs) {
		results.add(src);
		otherResults.add(otherArgs);
		currentResult = src;
	}
	public List<Mat> getAllResults(){
		return results;
	}
	@Override
	public Map<String, String> getConfigurationMap() {
		return configurationMap;
	}
	@Override
	public ValveChain setConfigurationKeyWithValue(String key,String value) {
		configurationMap.put(key, value);
		return this;
	}
	@Override
	public List<Object[]> getAllOtherResults() {
		return otherResults;
	}
	@Override
	public Object[] getFinalOtherResult() {
		return otherResults.get(otherResults.size()-1);
	}
}
