/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.core;

import java.util.List;

import org.opencv.core.Mat;

import cn.edu.bjtu.imageprocess.annotation.Stateful;
import cn.edu.bjtu.imageprocess.api.Pipeline;
import cn.edu.bjtu.imageprocess.api.ValveChain;

/**
 * @author Alex
 *
 */
@Stateful
public class StandardImagePipeline implements Pipeline {
	Mat src;
	ValveChain vc;
	public StandardImagePipeline(){
		vc = new ImageValveChain(
				new GrayValve(),   //灰度化
				new GaussianBlurValve(), //模糊
				new ThresholdValve(), //阈值化
				new CannyValve(), // 边缘检测
				new CentroidValve(), //找质心
				new TimeSeresConverterValve() //将边缘转化成时间序列
				);
	}
	@Override
	public void reset() {
		vc.reset();
	}

	@Override
	public List<Mat> getAllResults() {
		return vc.getAllResults();
	}
	@Override
	public Mat getFinalResult() {
		return vc.getOutput();
	}
	@Override
	public ValveChain getValveChain() {
		return vc;
	}
	@Override
	public void setInput(Mat m) {
		this.src = m;
	}
	@Override
	public void action() {
		vc.transformNext(src);
	}
	@Override
	public List<Object[]> getAllOtherResults() {
		return vc.getAllOtherResults();
	}
	@Override
	public Object[] getFinalOtherResult() {
		return vc.getFinalOtherResult();
	}

}
