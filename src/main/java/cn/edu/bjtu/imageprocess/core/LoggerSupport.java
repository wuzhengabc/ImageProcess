/**
 * 2017年5月17日
 */
package cn.edu.bjtu.imageprocess.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Alex
 *
 */
public abstract class LoggerSupport {
	protected Logger logger  = LoggerFactory.getLogger(this.getClass());
	protected void logException(Throwable t) {
		logger.error("Exception occurs: {}",t.getMessage(),t);
	}
}
