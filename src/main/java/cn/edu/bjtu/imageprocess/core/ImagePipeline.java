/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.core;

import java.util.List;

import org.opencv.core.Mat;

import cn.edu.bjtu.imageprocess.annotation.Stateful;
import cn.edu.bjtu.imageprocess.api.Pipeline;
import cn.edu.bjtu.imageprocess.api.Valve;
import cn.edu.bjtu.imageprocess.api.ValveChain;

/**
 * @author Alex
 *
 */
@Stateful
public class ImagePipeline implements Pipeline{
	Mat src;
	ValveChain vc;
	/**
	 * 该Pipeline所要经历的处理流程 
	 */
	public ImagePipeline(Valve ... vals) {
		vc = new ImageValveChain(vals);
	}
	public void setInput(Mat src){
		this.src = src;
	}
	public void action(){
		vc.transformNext(src);
	}
	public void reset() {
		vc.reset();
	}
	public List<Mat> getAllResults(){
		return vc.getAllResults();
	}
	public ValveChain getValveChain(){
		return vc;
	}
	@Override
	public Mat getFinalResult() {
		return vc.getOutput();
	}
	@Override
	public List<Object[]> getAllOtherResults() {
		return vc.getAllOtherResults();
	}
	@Override
	public Object[] getFinalOtherResult() {
		return vc.getFinalOtherResult();
	}
}
