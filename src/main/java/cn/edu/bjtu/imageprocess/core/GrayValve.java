/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.core;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import cn.edu.bjtu.imageprocess.annotation.Stateless;
import cn.edu.bjtu.imageprocess.api.Valve;
import cn.edu.bjtu.imageprocess.api.ValveChain;

/**
 * 灰度化
 * @author Alex
 *
 */
@Stateless
public class GrayValve implements Valve{
	public void transform(Mat src, ValveChain vc ,Object ...args) {
		try{
			Mat res = new Mat();
			Imgproc.cvtColor(src, res, Imgproc.COLOR_BGR2GRAY);
			vc.transformNext(res);
		}catch(Exception e){
			vc.handleException(e, src);
		}
	}

}
