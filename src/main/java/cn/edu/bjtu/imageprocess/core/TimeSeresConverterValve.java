/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;

import cn.edu.bjtu.imageprocess.annotation.Stateless;
import cn.edu.bjtu.imageprocess.api.Valve;
import cn.edu.bjtu.imageprocess.api.ValveChain;

/**
 * @author Alex
 *
 */
@Stateless
public class TimeSeresConverterValve implements Valve{
	public final static class MyPoint implements Comparable<MyPoint>{
		public MyPoint(int x, int y, double distance, double angle) {
			super();
			this.x = x;
			this.y = y;
			this.distance = distance;
			this.angle = angle;
		}
		public int x;
		public int y;
		public double distance;
		public double angle;
		@Override
		public int compareTo(MyPoint o) {
			int res = Double.compare(this.angle, o.angle);
			return res==0?Double.compare(this.distance, o.distance):res;
		}
		public String toString(){
			return "{"+angle+","+distance+","+x+","+y+"}";
		}
	}
	@Override
	public void transform(Mat src, ValveChain vc, Object... otherArgs) {
		Size s = src.size();
		int width = (int) s.width;
		int height  = (int) s.height;
		//获取质心,由上一个Valve传递过来
		Point centroid = (Point)otherArgs[0];
		double cX = centroid.x;
		double cY = centroid.y;
		List<MyPoint> list = new ArrayList<MyPoint>(width*height/10);
		//遍历src中值不为0的点,记录其位置及质心之间的距离
		for(int row = 0;row<height;row++){
			for(int col=0;col<width;col++){
				//对于灰度图来说,p长度为1,如果为RGB图,p的长度是,如果是RGBA,则p的长度是4
				//只有灰度图验证过,其他的图没有验证过
				double []p = src.get(row, col);
				if(p[0]>0){
					list.add(new MyPoint(col,row,calcDistance(col, row, cX, cY),calcAngle(col,row,cX,cY)));
				}
			}
		}
		//list中是每个不为0的点与质心之间的距离,及夹角
		Collections.sort(list);
		vc.transformTerminal(src, list,centroid);
	}
	private double calcDistance(int x,int y ,double cX,double cY){
		double dx = (double)x;
		double dy = (double)y;
		return Math.sqrt((dx-cX)*(dx-cX)+(dy-cY)*(dy-cY));
	}
	private double calcAngle(int x,int y ,double cX,double cY){
		double dx = (double)x;
		double dy = (double)y;
		double t1 = dx-cX;
		double t2 = dy-cY;
		double t = t2/t1;
		double res = Math.atan(t);
		return res;
	}
	

}
