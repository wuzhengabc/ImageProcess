/**
 * 2017年9月22日
 */
package cn.edu.bjtu.imageprocess.driver;

import java.util.List;
import java.util.Map;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import cn.edu.bjtu.imageprocess.api.ValveChain;
import cn.edu.bjtu.imageprocess.core.StandardImagePipeline;
import cn.edu.bjtu.imageprocess.image.ImageViewer;

/**
 * @author Alex
 *
 */
public class IPTest {
	public static void main(String[] args) {
		System.load("/File/Java/ImageProcess/src/main/lib/libopencv_java330.dylib");
		StandardImagePipeline pp = new StandardImagePipeline();
		ValveChain vc = pp.getValveChain();
		vc.setConfigurationKeyWithValue("gray.threshold", "0.5").setConfigurationKeyWithValue("scale.ratio", "0.2");
		Map<String,String> map = vc.getConfigurationMap();
		System.out.println(map);
		Mat src = Imgcodecs.imread("/File/Java/ImageProcess/src/main/webapp/timeseries/img/a1.jpg");
		pp.setInput(src);
		pp.action();
		Mat res = pp.getFinalResult();
		//showImg(res, "canny img");
		Object [] listArr = pp.getFinalOtherResult();
		List l = (List)listArr[0];
		Point p = (Point)listArr[1];
		Imgproc.drawMarker(res, p, new Scalar(255));
		showImg(res, "Point edge");
		for(Object o:l){
			System.out.println(o);
		}
	}
	static void showImg(Mat m,String name){
		ImageViewer iv = new ImageViewer(m,name);
		iv.imshow();
	}
}
