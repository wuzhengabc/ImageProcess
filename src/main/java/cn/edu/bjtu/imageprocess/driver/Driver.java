/**
 * 2017年9月21日
 */
package cn.edu.bjtu.imageprocess.driver;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import cn.edu.bjtu.imageprocess.image.ImageViewer;



/**
 * @author Alex
 *
 */
public class Driver {
	public static void main(String[] args) {
		System.load("/File/Java/ImageProcess/src/main/lib/libopencv_java330.dylib");
		Mat src = Imgcodecs.imread("/File/Java/ImageProcess/src/main/webapp/timeseries/img/a1.jpg");
		Mat dst = newMat();
		Mat thresholdedMat = newMat();
		//灰度化
		Imgproc.cvtColor(src, dst, Imgproc.COLOR_BGR2GRAY);
		//阈值分割
		
		Imgproc.threshold(dst, thresholdedMat, 128, 255, Imgproc.THRESH_BINARY);
		showImg(thresholdedMat,"threshold");
		System.out.println("size:"+dst.size()+",type:"+dst.type());
		Mat bluredImg = new Mat();
		//高斯模糊
		Imgproc.GaussianBlur(thresholdedMat, bluredImg, new Size(15,15), 0.5);
		
		Mat canniedImg = new Mat();
		//Canny边缘检测
		Imgproc.Canny(bluredImg, canniedImg, 0.7, 0.7);
//		Imgproc.moments(array)
		showImg(src,"src");
		showImg(dst,"dst");
		showImg(bluredImg,"blur");
		showImg(canniedImg,"cannied");
		
		List<MatOfPoint> contours = new ArrayList<>();
		//寻找图像的矩
		Moments ms = Imgproc.moments(canniedImg);
		Mat hierarchy = new Mat();
		Imgproc.findContours(canniedImg, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_NONE);
//		Imgproc.circle(img, center, radius, color);
		//画矩,未知
		Mat drawContours = Mat.zeros(dst.size(),thresholdedMat.type());
		//Imgproc.drawContours(drawContours, contours,dst.type() , new Scalar(0.5));
		//这个点就是质心的位置
		Point p = new Point(ms.m10/ms.m00,ms.m01/ms.m00);
		Imgproc.drawMarker(drawContours, p, new Scalar(255));
		showImg(drawContours, "contours");
		System.out.println("he");
//		int width = (int)canniedImg.size().width;
//		int height = (int)canniedImg.size().height;
//		for(int i =0 ;i<width;i++){
//			for(int j = 0;j<height;j++){
//				System.out.print(Arrays.toString(canniedImg.get(j, i)));
//				System.out.print(" ");
//			}
//			System.out.println();
//		}
	}
	static void showImg(Mat m,String name){
		ImageViewer iv = new ImageViewer(m,name);
		iv.imshow();
	}
	static Mat newMat(){
		return new Mat();
	}
}
