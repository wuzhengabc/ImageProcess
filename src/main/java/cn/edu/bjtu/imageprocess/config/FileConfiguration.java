/**
 * 2017年8月17日
 */
package cn.edu.bjtu.imageprocess.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Alex
 *
 */
@ConfigurationProperties(locations="classpath:configuration/file.properties")
@Component
@Scope("singleton")
public class FileConfiguration {
	String alex;
	String arffDir;
	String imgDir;
    String resultImgDir;
    String classDir;

	public String getClassDir() {
		return classDir;
	}
	public void setClassDir(String classDir) {
		this.classDir = classDir;
	}
	public String getAlex() {
		return alex;
	}
	public void setAlex(String alex) {
		this.alex = alex;
	}

	public String getArffDir() {
		return arffDir;
	}
	public void setArffDir(String arffDir) {
		this.arffDir = arffDir;
	}

	public String getImgDir() { return imgDir; }
	public void setImgDir(String imgDir) { this.imgDir = imgDir; }

    public String getResultImgDir() { return resultImgDir; }
    public void setResultImgDir(String resultImgDir) { this.resultImgDir = resultImgDir; }
}
