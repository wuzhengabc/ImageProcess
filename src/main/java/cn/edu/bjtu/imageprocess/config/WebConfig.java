/**
 * 2017年7月22日
 */
package cn.edu.bjtu.imageprocess.config;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import cn.edu.bjtu.imageprocess.core.LoggerSupport;

/**
 * @author Alex
 *
 */
@Import(MvcConfig.class)
public class WebConfig extends LoggerSupport{
	  @Bean
	  public ServletRegistrationBean servletRegistrationBean() {
        return new ServletRegistrationBean(new Servlet() {
			
			@Override
			public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
				logger.info("{}",req);
				res.getWriter().println("res");
			}
			
			
			@Override
			public void init(ServletConfig config) throws ServletException {
				WebMvcConfigurerAdapter f;
			}
			
			@Override
			public String getServletInfo() {
				return "anomyous servlet";
			}
			
			@Override
			public ServletConfig getServletConfig() {
				return null;
			}
			
			@Override
			public void destroy() {
			}
		}, "/xs/*");// ServletName默认值为首字母小写，即myServlet
    }

}
