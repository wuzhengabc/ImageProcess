/**
 * 2017年8月13日
 */
package cn.edu.bjtu.imageprocess.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import cn.edu.bjtu.imageprocess.core.StandardImagePipeline;
import weka.core.converters.JSONSaver;

/**
 * @author Alex
 *
 */
@Configuration
public class ComponentsConfig extends WebMvcConfigurerAdapter {
	@Bean
	ThreadLocal<JSONSaver> jsonSavers(){
		return new ThreadLocal<JSONSaver>(){
			@Override
			protected JSONSaver initialValue() {
				return new JSONSaver();
			}
			@Override
			public JSONSaver get() {
				JSONSaver js = super.get();
				js.resetWriter();
				js.resetOptions();
				js.resetStructure();
				return js;
			}
		};
	}
	@Bean
	ThreadLocal<StandardImagePipeline> imagePipelines(){
		return new ThreadLocal<StandardImagePipeline>(){
			protected StandardImagePipeline initialValue() {
				return new StandardImagePipeline();
			}
			@Override
			public StandardImagePipeline get() {
				StandardImagePipeline res =  super.get();
				res.reset();
				return res;
			}
		};
	}
}
