'use strict';

//**********************self defined controller begin****************************
app.controller('ClassifiersController', ['$scope','$http', '$modal',function($scope,$http,$modal) {
	//$scope.content = ""aaaa;
	$scope.name = ["Google", "Runoob", "Taobao"];
	$scope.classifiersResult = "这里显示分类器数据";
    $scope.options = {
        sAjaxSource: '/tsc/files',
        aoColumns: [
            { mData: 'name' },
            { mData: 'size' }
        ],
        aoColumnDefs: [
        	{
                "aTargets" :[1],
                "mRender" : function(data, type, full) {
                    return (data/1024).toFixed(2) + "KB";
                }
            },
           {
               "aTargets" :[2],
               "mRender" : function(data, type, full) {
                   return "<select class='btn btn-default form-control ui-select-match'></select>";
               }
           },
            {
                "aTargets" :[3],
                "mRender" : function(data, type, full) {
                    return "<button  class='btn btn-sm btn-default'>建立分类器</button>";
                }
            },
            {
            	"aTargets" :[4],
                "mRender" : function(data, type, full) {
                    return "<button class='btn btn-sm btn-default'>选择</button>";
                }
            }
        ],
        rowCallback:function(r){
            var data = arguments[1].name;
            if(!(data.substring(data.indexOf("_") + 1, data.indexOf(".")) == "TRAIN" || data.substring(data.indexOf("_") + 1, data.indexOf(".")) == "TEST"))
            {
                var openbtn =$("td:eq(3) button",r).hide();
                var btn =$("td:eq(2) select",r).hide();
                var btn = $("td:last button:last",r).hide();
            }
            $scope.filename = data;
            var selectedalgo;
            var openbtn =$("td:eq(3) button",r).click(function(){
            	if(selectedalgo==null)
            		{
	            		$scope.items=["请选择分类器算法"];	
	      			    $scope.open();
            		}//'/tsc/create/'+$scope.aaa
            	else
            	  $http({
            		  url:'/tsc/create/'+data,
            		  method : 'get',
            		  params:{algo:selectedalgo}
            	  }).success(function(data,status,headers,config){
            		var con = JSON.stringify(data);
            		var error = data['error'];
            		if(error=="error1")
            		{
            			$scope.items=["分类器已存在"];	
          			    $scope.open();
            		}
            		else{
	            		$scope.names = data;
	            		$scope.items=["分类器建立成功"];	
	      			    $scope.open(); 
	      			    $http.get('/tsc/classifiers/files').success(function(data,status,headers,config){
	      			    	$scope.employees = data;
	      			  });
	      			    }         		
        		  })
            });
            var btn = $("td:last button:last",r).click(function() {
      			    $scope.selectInstances =  data;
      			    $scope.items=["数据集选择成功"];	
      			    $scope.open();
      			    
            });
            
            $("td:eq(2) select",r).append("<option value='Value'>NaiveBayes</option>");
            $("td:eq(2) select",r).append("<option value='Value1'>IBk</option>");
            
            var btn =$("td:eq(2) select",r).change(function(){
            	var selected = $("td:eq(2) select",r).find("option:selected").text();
            	selectedalgo = selected;
            	
            });
            selectedalgo = $("td:eq(2) select",r).find("option:selected").text();;
        }
    };
    
    $http.get('/tsc/classifiers/files').success(function(data,status,headers,config){
    	$scope.employees = data;
	  });
   $scope.deleteClassifier = function(classifiersName){
	 /*  
	   if(confirm("确定删除" + classifiersName + "吗？")) {
           $http.get('/tsc/deleteClassifiers/'+classifiersName).success(function(data,status,headers,config){
               $http.get('/tsc/classifiers/files').success(function(data,status,headers,config){
                   $scope.employees = data;
               });
           });
       }*/
	   $scope.items=['确定删除'+ classifiersName+'分类器吗？'];
	   var modalInstance = $modal.open({
  	        templateUrl: 'myModalContent.html',
  	        controller: 'ModalInstanceCtrl',
  	        size: 'sm',
  	        resolve: {
  	          items: function () {
  	            return $scope.items;
  	          }
  	        }
  	      }).result.then(function (selectedItem) {
  	    	 $http.get('/tsc/deleteClassifiers/'+classifiersName).success(function(data,status,headers,config){
                 $http.get('/tsc/classifiers/files').success(function(data,status,headers,config){
                     $scope.employees = data;
                 });
             });
  	      }, function () {
  	      });
   }
   $scope.choose = function(a,b){
	   
		var name = $scope.selectInstances;
	   	if(!name){
	   		$scope.items = ['请选择数据集'];
	   		$scope.open();
	   		}
	   	else
	   		{
	   			$http({
	   				url:'/tsc/testClassifiers/'+name,
	   				method:'get',
	   				params:{classifier : a+""}
	   			}).success(function(data,status,headers,config){
	    			var result1 = data['roc'];
	    			var result2 = data['other'];
	           	 var d = result1.map(function(item){
	           		 return [item.tpRate,item.fpRate];
	           	 });
	           	 $scope.eva = result2[0];
		          $scope.data7 = [{
		                  data: d,
		                  label: "ROC",
		                  points: { show: true},
		                  splines: { show : true ,tension: 0.4, lineWidth: 1, fill: 0.8 }
		              }];
		   		});
	   		}
	   	
   }

    $scope.open = function(){
   	 var modalInstance = $modal.open({
   	        templateUrl: 'myModalContent.html',
   	        controller: 'ModalInstanceCtrl',
   	        size: 'sm',
   	        resolve: {
   	          items: function () {
   	            return $scope.items;
   	          }
   	        }
   	      });
   	      modalInstance.result.then(function (selectedItem) {
   	        $scope.selected = selectedItem;
   	      }, function () {
   	        $log.info('Modal dismissed at: ' + new Date());
   	      });
    }
    
    $scope.data7=[
                 { data: [ [0,0],[0.1,0.1],[0.2,0.5],[0.3,0.7],[0.4,0.9],[0.6,0.8],[1,1]], label: 'Roc',  points: { show: true, radius: 1}, splines: { show : true ,tension: 0.4, lineWidth: 1, fill: 0.8 } },
             ];
    $scope.eva={"kappa":0.0,"meanAbsErr":0.0,"precision":0.0,"recall":0.0,"auc":0.0,"correct":50,"incorrect":50};
   
    $scope.test = function(){
    	var name = $scope.selectInstances;
    	if(!name)
    	{
    		$scope.items=["请先选择数据集和分类器后在进行测试"];	
		    $scope.open();
    	}
    	else
    		$http.get('/tsc/testClassifiers/'+name).success(function(data,status,headers,config){
    			var result1 = data['roc'];
    			var result2 = data['other'];
           	 var d = result1.map(function(item){
           		 return [item.tpRate,item.fpRate];
           	 });
           	 $scope.eva = result2[0];
	          $scope.data7 = [{
	                  data: d,
	                  label: "ROC",
	                  points: { show: true},
	                  splines: { show : true ,tension: 0.4, lineWidth: 1, fill: 0.8 }
	              }];
	     });
	   }
  
}]);

//**********************self defined controller end****************************


var processtype = {gray: 'gray', blur:'blur', threshold: 'threshold', canny: 'canny', curve: 'curve'};
//**********************self defined controller begin****************************
app.controller('ImageProcessController', ['$scope','$http', function($scope,$http) {
    $scope.transformedData = "这里显示转化之后的数据";
    $scope.originalImg="../timeseries/emptyImg.jpg";
    $scope.$apply();
    initialize();
    $scope.blurSize = 15;
    $scope.sigmaX = 0.5;
    $scope.thresholdThresh = 200;
    $scope.thresholdMaxVal = 255;
    $scope.cannyThreshold1 = 0.7;
    $scope.cannyThreshold2 = 0.7;

    function initialize() {

        $scope.disableblur = true;
        $scope.disablethreshold = true;
        $scope.disablecanny = true;
        $scope.disablecurve = true;
        //默认图片
        $scope.grayImg="../timeseries/emptyImg.jpg";
        $scope.blurImg="../timeseries/emptyImg.jpg";
        $scope.thresholdImg="../timeseries/emptyImg.jpg";
        $scope.cannyImg="../timeseries/emptyImg.jpg";
        $scope.data=[
            { data: [], label: 'Data', points: { show: true, radius: 2 },lines: { show: true, fill: true, fillColor: { colors: [{ opacity: 0.1 }, { opacity: 0.1}] } } }
        ];
        $scope.$apply();
    }
    
    $scope.options = {
        sAjaxSource: '/tsc/img/files',
        aoColumns: [
            { mData: 'name' },
            { mData: 'size' }
        ],
        aoColumnDefs: [
            {
                "aTargets" :[1],
                "mRender" : function(data, type, full){

                    return (data/1024).toFixed(2) + "KB";
                }
            },
            {
                "aTargets" :[2],
                "mRender" : function(data, type, full) {
                    return "<button class='btn btn-info'>加载图片</button>";
                }
            }
        ],
        rowCallback:function(r){
        	//文件名  例如xxx.jpg
            var data = arguments[1].name;
            var btn = $("td:last button",r).click(function() {
                $scope.filename = data;

                $scope.disablegray = false;

                initialize();
                $scope.originalImg="/tsc/img/"+data;
                $scope.$apply();
            });
        }
    };

    var tempdata = [];
    $scope.convert = function(pt){
        var name = $scope.filename;
        if(!name) return;

        var size = $scope.blurSize;
        if(size % 2 == 0)
            $scope.blurSize = size + 1;
        if(pt == processtype.gray)
        {
            initialize();
            $http({
                method: "get",
                params:{
                    name:name,
                    blursize:$scope.blurSize,
                    sigmax:$scope.sigmaX,
                    thresholdthresh:$scope.thresholdThresh,
                    thresholdmaxVal:$scope.thresholdMaxVal,
                    cannythreshold1:$scope.cannyThreshold1,
                    cannythreshold2:$scope.cannyThreshold2

                },
                url:"/tsc/img/serialize"
            }).success(function(data,status,headers,config) {
                tempdata = data;
                $scope.grayImg="../timeseries/result/Gray" + name + "?v=" + Math.random();
                $scope.$apply();
                $scope.disableblur = false;
            });
            // $http.get('/tsc/img/serialize?name='+name+'&blursize='+blursize).success(function(data,status,headers,config) {
            //     tempdata = data;
            //     $scope.grayImg="../timeseries/result/Gray" + name;
            //     $scope.$apply();
            //     $scope.disableblur = false;
            // });
        }
        var index = name.lastIndexOf(".");
        var extend = name.substr(index+1).toLowerCase();
        var fileName = name.substr(0,name.lastIndexOf("."));
        if(extend == "gif")
        {
            name = fileName + ".jpg";
        }
        if(pt == processtype.blur)
        {
            $scope.blurImg="../timeseries/result/Blur" + name + "?v=" + Math.random();
            $scope.$apply();
            $scope.disablethreshold = false;
        }
        if(pt == processtype.threshold)
        {
            $scope.thresholdImg="../timeseries/result/Threshold" + name + "?v=" + Math.random();
            $scope.$apply();
            $scope.disablecanny = false;
        }
        if(pt == processtype.canny)
        {
            $scope.cannyImg="../timeseries/result/Canny" + name + "?v=" + Math.random();
            $scope.$apply();
            $scope.disablecurve = false;

        }
        if(pt == processtype.curve)
        {
            var d = tempdata.map(function (item, idx) {
                return [idx, item.distance];
            });
            $scope.transformedData = tempdata;
            $scope.data = [{
                data: d,
                label: "Data",
                points: { show: true, radius: 2 },
                lines: { show: true, fill: true, fillColor: { colors: [{ opacity: 0.1 }, { opacity: 0.1}] } }
            }];
        }
    };

    $scope.next=function(){
        window.d = $scope.dataset;
        if($scope.dataset){
            var res = $scope.dataset.data.map(function(item,index){
                var lable = item.values.slice(-1);
                return {
                    data: item.values.slice(0,-1).map(function(x,idx){return [idx,parseFloat(x)];}),
                    label: lable,
                    points: { show: true, radius: 2 },
                    lines: { show: true, fill: true, fillColor: { colors: [{ opacity: 0.1 }, { opacity: 0.1}] } }
                }
            });
            $scope.data= res.slice(0,10);
        }

    }
    //"[50.32,45.23,47.56,36.25,53.85,40.15,41.25,50.15,57.14,36.15,97.26,50.15,45.32,47.19,37.75,25.15,56.34,50.35,47.25,53.15],{type:'line', height:65, width: '100%', lineWidth:2, valueSpots:{'0:':'gray'}, lineColor:'black', spotColor:'black', fillColor:'', highlightLineColor:'black', spotRadius:3}";
    $scope.cavansOptions = {
        type:'line',
        height:65,
        width: '100%',
        lineWidth:2,
        valueSpots:{'0:':'gray'},
        lineColor:'black',
        spotColor:'black',
        fillColor:'',
        highlightLineColor:'black',
        spotRadius:3
    };

}]);

//**********************self defined controller end****************************

//**********************self defined controller begin**************************** 
app.controller('ArffController', ['$scope','$http', '$modal',function($scope,$http,$modal) {
    $http.get('/tsc/files').success(function(data,status,headers,config){
        $scope.arffdata = data.aaData;
    });

	$scope.options = {
      sAjaxSource: '/tsc/files',
      aoColumns: [
        { mData: 'name' },
        { mData: 'size' }
      ],
      aoColumnDefs: [
          {
              "aTargets" :[1],
              "mRender" : function(data, type, full){

                  return (data/1024).toFixed(2) + "KB";
              }
          },
            {
				"aTargets" :[2],
				"mRender" : function(data, type, full){
				    var btns = "<button id='open' class='btn btn-info' style='margin-right: 5px'>打开</button>" +
                        "<button id='delete' class='btn btn-info'>删除</button>";
				    return btns;
				}
			}

      ],
      rowCallback:function(r){
    	  var data = arguments[1].name;
    	  var btn = $("#open",r).click(function(){
    		  $http.get('/tsc/file/'+data).success(function(data,status,headers,config){
                  $scope.dataset = data;
                  window.d = $scope.dataset;
                  if($scope.dataset){
                      var res = $scope.dataset.data.map(function(item,index){
                          var lable = item.values.slice(-1);
                          return {
                              data: item.values.slice(0,-1).map(function(x,idx){return [idx,parseFloat(x)];}),
                              label: lable,
                              points: { show: true, radius: 2 },
                              lines: { show: true, fill: true, fillColor: { colors: [{ opacity: 0.1 }, { opacity: 0.1}] } }
                          }
                      });
                      $scope.data= res.slice(0,10);
                  }
              });
    	  });

          var btn = $("#delete",r).click(function(){

          });
      }
	};

    $scope.data=[
        { data: [], label: 'Data', points: { show: true, radius: 2 },lines: { show: true, fill: true, fillColor: { colors: [{ opacity: 0.1 }, { opacity: 0.1}] } } }
    ];
	//"[50.32,45.23,47.56,36.25,53.85,40.15,41.25,50.15,57.14,36.15,97.26,50.15,45.32,47.19,37.75,25.15,56.34,50.35,47.25,53.15],{type:'line', height:65, width: '100%', lineWidth:2, valueSpots:{'0:':'gray'}, lineColor:'black', spotColor:'black', fillColor:'', highlightLineColor:'black', spotRadius:3}";
	$scope.cavansOptions = {
			type:'line',
			height:65,
			width: '100%',
			lineWidth:2,
			valueSpots:{'0:':'gray'},
			lineColor:'black',
			spotColor:'black',
			fillColor:'',
			highlightLineColor:'black',
			spotRadius:3
		};

    $scope.openFile = function (fileName) {
        $http.get('/tsc/file/'+fileName).success(function(data,status,headers,config){
            $scope.dataset = data;
            window.d = $scope.dataset;
            if($scope.dataset){
                var res = $scope.dataset.data.map(function(item,index){
                    var lable = item.values.slice(-1);
                    return {
                        data: item.values.slice(0,-1).map(function(x,idx){return [idx,parseFloat(x)];}),
                        label: lable,
                        points: { show: true, radius: 2 },
                        lines: { show: true, fill: true, fillColor: { colors: [{ opacity: 0.1 }, { opacity: 0.1}] } }
                    }
                });
                $scope.data= res.slice(0,10);
            }
        });
    };

    $scope.deleteFile = function (fileName) {
        $scope.items=['确定删除'+ fileName+'分类器吗？'];
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: 'sm',
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        }).result.then(function (selectedItem) {
            $http.get('/tsc/delete/'+fileName).success(function(data,status,headers,config){
                $http.get('/tsc/files').success(function(data,status,headers,config){
                    $scope.arffdata = data.aaData;
                });
            });
        }, function () {
        });
    };

    $scope.batchConvert = function(){
        var datasetName = $scope.datasetName;
        if(!(datasetName == null || datasetName == "" || datasetName == "undefined"))
        {
            $http.get('/tsc/img/batchconvert?datasetname=' + datasetName).success(function(data,status,headers,config) {
                alert("转换成功" + data);
                $http.get('/tsc/files').success(function(data,status,headers,config){
                    $scope.arffdata = data.aaData;
                });
            });
        }
    };

    $scope.normalization = function(){
        $http.get('/tsc/img/normalization').success(function(data,status,headers,config) {
            alert("归一化成功");
            $http.get('/tsc/files').success(function(data,status,headers,config){
                $scope.arffdata = data.aaData;
            });
        });
    };

    $scope.splitDataSet = function(){
        $http.get('/tsc/img/splitdataset').success(function(data,status,headers,config) {
            alert("数据集分割完成");
            $http.get('/tsc/files').success(function(data,status,headers,config){
                $scope.arffdata = data.aaData;
            });
        });
    };
	
}]); 

//**********************self defined controller end****************************
/* Controllers */
  // bootstrap controller
  app.controller('AccordionDemoCtrl', ['$scope', function($scope) {
    $scope.oneAtATime = true;

    $scope.groups = [
      {
        title: 'Accordion group header - #1',
        content: 'Dynamic group body - #1'
      },
      {
        title: 'Accordion group header - #2',
        content: 'Dynamic group body - #2'
      }
    ];

    $scope.items = ['Item 1', 'Item 2', 'Item 3'];

    $scope.addItem = function() {
      var newItemNo = $scope.items.length + 1;
      $scope.items.push('Item ' + newItemNo);
    };

    $scope.status = {
      isFirstOpen: true,
      isFirstDisabled: false
    };
  }])
  ; 
  app.controller('AlertDemoCtrl', ['$scope', function($scope) {
    $scope.alerts = [
      { type: 'success', msg: 'Well done! You successfully read this important alert message.' },
      { type: 'info', msg: 'Heads up! This alert needs your attention, but it is not super important.' },
      { type: 'warning', msg: 'Warning! Best check yo self, you are not looking too good...' }
    ];

    $scope.addAlert = function() {
      $scope.alerts.push({type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.'});
    };

    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
  }])
  ; 
  app.controller('ButtonsDemoCtrl', ['$scope', function($scope) {
    $scope.singleModel = 1;

    $scope.radioModel = 'Middle';

    $scope.checkModel = {
      left: false,
      middle: true,
      right: false
    };
  }])
  ; 
  app.controller('CarouselDemoCtrl', ['$scope', function($scope) {
    $scope.myInterval = 5000;
    var slides = $scope.slides = [];
    $scope.addSlide = function() {
      slides.push({
        image: 'img/c' + slides.length + '.jpg',
        text: ['Carousel text #0','Carousel text #1','Carousel text #2','Carousel text #3'][slides.length % 4]
      });
    };
    for (var i=0; i<4; i++) {
      $scope.addSlide();
    }
  }])
  ; 
  app.controller('DropdownDemoCtrl', ['$scope', function($scope) {
    $scope.items = [
      'The first choice!',
      'And another choice for you.',
      'but wait! A third!'
    ];

    $scope.status = {
      isopen: false
    };

    $scope.toggled = function(open) {
      //console.log('Dropdown is now: ', open);
    };

    $scope.toggleDropdown = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.status.isopen = !$scope.status.isopen;
    };
  }])
  ; 
  app.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', 'items', function($scope, $modalInstance, items) {
    $scope.items = items;
    $scope.selected = {
      item: $scope.items[0]
    };

    $scope.ok = function () {
      $modalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }])
  ; 
  app.controller('ModalDemoCtrl', ['$scope', '$modal', '$log', function($scope, $modal, $log) {
    $scope.items = ['item1', 'item2', 'item3'];
    $scope.open = function (size) {
      var modalInstance = $modal.open({
        templateUrl: 'myModalContent.html',
        controller: 'ModalInstanceCtrl',
        size: size,
        resolve: {
          items: function () {
            return $scope.items;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };
  }])
  ; 
  app.controller('PaginationDemoCtrl', ['$scope', '$log', function($scope, $log) {
    $scope.totalItems = 64;
    $scope.currentPage = 4;

    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function() {
      $log.info('Page changed to: ' + $scope.currentPage);
    };

    $scope.maxSize = 5;
    $scope.bigTotalItems = 175;
    $scope.bigCurrentPage = 1;
  }])
  ; 
  app.controller('PopoverDemoCtrl', ['$scope', function($scope) {
    $scope.dynamicPopover = 'Hello, World!';
    $scope.dynamicPopoverTitle = 'Title';
  }])
  ; 
  app.controller('ProgressDemoCtrl', ['$scope', function($scope) {
    $scope.max = 200;

    $scope.random = function() {
      var value = Math.floor((Math.random() * 100) + 1);
      var type;

      if (value < 25) {
        type = 'success';
      } else if (value < 50) {
        type = 'info';
      } else if (value < 75) {
        type = 'warning';
      } else {
        type = 'danger';
      }

      $scope.showWarning = (type === 'danger' || type === 'warning');

      $scope.dynamic = value;
      $scope.type = type;
    };
    $scope.random();

    $scope.randomStacked = function() {
      $scope.stacked = [];
      var types = ['success', 'info', 'warning', 'danger'];

      for (var i = 0, n = Math.floor((Math.random() * 4) + 1); i < n; i++) {
          var index = Math.floor((Math.random() * 4));
          $scope.stacked.push({
            value: Math.floor((Math.random() * 30) + 1),
            type: types[index]
          });
      }
    };
    $scope.randomStacked();
  }])
  ; 
  app.controller('TabsDemoCtrl', ['$scope', function($scope) {
    $scope.tabs = [
      { title:'Dynamic Title 1', content:'Dynamic content 1' },
      { title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
    ];
  }])
  ; 
  app.controller('RatingDemoCtrl', ['$scope', function($scope) {
    $scope.rate = 7;
    $scope.max = 10;
    $scope.isReadonly = false;

    $scope.hoveringOver = function(value) {
      $scope.overStar = value;
      $scope.percent = 100 * (value / $scope.max);
    };
  }])
  ; 
  app.controller('TooltipDemoCtrl', ['$scope', function($scope) {
    $scope.dynamicTooltip = 'Hello, World!';
    $scope.dynamicTooltipText = 'dynamic';
    $scope.htmlTooltip = 'I\'ve been made <b>bold</b>!';
  }])
  ; 
  app.controller('TypeaheadDemoCtrl', ['$scope', '$http', function($scope, $http) {
    $scope.selected = undefined;
    $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
    // Any function returning a promise object can be used to load values asynchronously
    $scope.getLocation = function(val) {
      return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
        params: {
          address: val,
          sensor: false
        }
      }).then(function(res){
        var addresses = [];
        angular.forEach(res.data.results, function(item){
          addresses.push(item.formatted_address);
        });
        return addresses;
      });
    };
  }])
  ; 
  app.controller('DatepickerDemoCtrl', ['$scope', function($scope) {
    $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
  }])
  ; 
  app.controller('TimepickerDemoCtrl', ['$scope', function($scope) {
    $scope.mytime = new Date();

    $scope.hstep = 1;
    $scope.mstep = 15;

    $scope.options = {
      hstep: [1, 2, 3],
      mstep: [1, 5, 10, 15, 25, 30]
    };

    $scope.ismeridian = true;
    $scope.toggleMode = function() {
      $scope.ismeridian = ! $scope.ismeridian;
    };

    $scope.update = function() {
      var d = new Date();
      d.setHours( 14 );
      d.setMinutes( 0 );
      $scope.mytime = d;
    };

    $scope.changed = function () {
      //console.log('Time changed to: ' + $scope.mytime);
    };

    $scope.clear = function() {
      $scope.mytime = null;
    };
  }]);