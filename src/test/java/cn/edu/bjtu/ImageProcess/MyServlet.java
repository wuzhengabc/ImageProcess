/**
 * 2017年9月13日
 */
package cn.edu.bjtu.ImageProcess;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * @author Alex
 *
 */
public class MyServlet implements Servlet{
	Map<String ,Object> m = new HashMap<>();
	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {

	
		req.startAsync();
		AsyncContext ac = req.getAsyncContext();
		ac.addListener(new AsyncListener() {
			
			@Override
			public void onTimeout(AsyncEvent event) throws IOException {
			}
			
			@Override
			public void onStartAsync(AsyncEvent event) throws IOException {
			}
			
			@Override
			public void onError(AsyncEvent event) throws IOException {
			}
			
			@Override
			public void onComplete(AsyncEvent event) throws IOException {
				event.getAsyncContext().getResponse().getWriter().println("ok");
			}
		});
		m.put("username",req.getParameter("username"));
	}

	
	
	@Override
	public void init(ServletConfig config) throws ServletException {
	}

	@Override
	public ServletConfig getServletConfig() {
		return null;
	}


	@Override
	public String getServletInfo() {
		return null;
	}

	@Override
	public void destroy() {
	}

}
